package Graph;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class GraphBfs {

	private int vertices;
	private LinkedList<Integer>[] adjacent;
	
	public GraphBfs(int vertices){
		this.vertices=vertices;
		this.adjacent=new LinkedList[vertices];
		
		for(int i=0;i<vertices;i++){
			
			adjacent[i]=new LinkedList<Integer>();
		}
		
	}
	
	public void addEdge(int v,int w){
		
		adjacent[v].add(w);
	}
	
	public static void main(String args[]){
		
		GraphBfs grah = new GraphBfs(4);

		grah.addEdge(0, 1);
		grah.addEdge(0, 2);
		grah.addEdge(1, 2);
		grah.addEdge(2, 0);
		grah.addEdge(2, 3);
		grah.addEdge(3, 3);

		System.out.println("Following is Depth First Traversal " + "(starting from vertex 2)");

		grah.printBfs(2);
		
	
	}
	
	public void printBfs(int edge){
		
		boolean[] visited=new boolean[4];
		
		Queue<Integer> queue=new LinkedList<Integer>();
		
		visited[edge]=true;
		
		queue.add(edge);
		
		while(!queue.isEmpty()){
			
		int val=queue.poll();
		System.out.println(val);
		
		Iterator<Integer> itr=adjacent[val].iterator();
		while(itr.hasNext()){
			
			int i=itr.next();
			
			if(!visited[i]){
				queue.add(i);
				visited[i]=true;
				
			}
		}
		}
		
		
		
	}
}
