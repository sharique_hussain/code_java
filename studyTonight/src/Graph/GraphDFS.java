package Graph;

import java.util.Iterator;
import java.util.LinkedList;

public class GraphDFS {

	private int vertices;

	private LinkedList<Integer>[] adjacenyList;

	public int getVertices() {
		return vertices;
	}

	public void setVertices(int vertices) {
		this.vertices = vertices;
	}

	public LinkedList<Integer>[] getAdjacenyList() {
		return adjacenyList;
	}

	public void setAdjacenyList(LinkedList<Integer>[] adjacenyList) {
		this.adjacenyList = adjacenyList;
	}

	public GraphDFS(int vertices) {

		this.vertices = vertices;
		adjacenyList = new LinkedList[vertices];

		for (int i = 0; i < vertices; i++) {

			adjacenyList[i] = new LinkedList<Integer>();
		}

	}

	public void addEdge(int v, int w) {

		adjacenyList[v].add(w);
	}

	public static void main(String args[]) {

		GraphDFS grah = new GraphDFS(4);

		grah.addEdge(0, 1);
		grah.addEdge(0, 2);
		grah.addEdge(1, 2);
		grah.addEdge(2, 0);
		grah.addEdge(2, 3);
		grah.addEdge(3, 3);

		System.out.println("Following is Depth First Traversal " + "(starting from vertex 2)");

		boolean[] visited = new boolean[4];
		
		grah.printDfsTraversal(2,visited);

	}

	public void printDfsTraversal(int vertice, boolean[] visited) {

		if (visited[vertice]) {

			return;
		}
		System.out.println(vertice);

		visited[vertice] = true;

		Iterator<Integer> value = adjacenyList[vertice].iterator();

		while (value.hasNext()) {

			int ver = value.next();

			if (!visited[ver]) {

				printDfsTraversal(ver, visited);
			}

		}
	}

}
