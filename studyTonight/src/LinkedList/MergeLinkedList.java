package LinkedList;

public class MergeLinkedList {

	Node head;

	public static void main(String args[]) {

		MergeLinkedList list1 = new MergeLinkedList();

		MergeLinkedList list2 = new MergeLinkedList();

		list1.head = new Node(3);
		list1.head.next = new Node(6);
		list1.head.next.next = new Node(15);
		list1.head.next.next.next = new Node(15);
		list1.head.next.next.next.next = new Node(30);

		// creating second linked list
		list2.head = new Node(10);
		list2.head.next = new Node(15);
		list2.head.next.next = new Node(30);

		int l1 = getCount(list1.head);
		int l2 = getCount(list2.head);

		if (l1 > l2) {
			System.out.println(getNode(list1.head, list2.head, l1 - l2));

		} else {
			System.out.println(getNode(list1.head, list2.head, l2 - l1));
		}

	}

	public static void reverse(Node head) {
	Node previous=null;
	Node current =head;
	Node next=null;
	while(current != null){
		next=current.next;
		current.next=previous;
		previous=current;
		current=next;
		
	}

}
	public static void recursiveReverse(Node head,Node current,Node prev){
		
		if(head.next==null){
			
			head=head.next;
			
		}
		
	}
	

	public static int getNode(Node l1, Node l2, int d) {

		Node current1 = l1;
		Node current2 = l2;
		for (int i = 0; i < d; i++) {
			if (current1 == null) {
				return -1;
			}
			current1 = current1.next;
		}
		while (current1 != null && current2 != null) {
			if (current1.data == current2.data) {
				return current1.data;
			}
			current1 = current1.next;
			current2 = current2.next;
		}

		return -1;
	}

	public static int getCount(Node head) {

		Node nod = head;
		if (head == null) {
			return 0;
		}
		int count = 0;
		while (nod != null) {

			count++;
			nod = nod.next;
		}

		return count;
	}
}
