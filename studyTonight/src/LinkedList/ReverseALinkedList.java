package LinkedList;

public class ReverseALinkedList {

	public static Node head;

	static class Node {

		Node next;
		int data;

		public Node(int value) {

			this.next = null;
			this.data = value;
		}
	}

	public static void main(String args[]) {

		ReverseALinkedList linkedList = new ReverseALinkedList();

		linkedList.head = new Node(1);
		linkedList.head.next = new Node(2);
		linkedList.head.next.next = new Node(3);
		linkedList.head.next.next.next = new Node(4);
		linkedList.head.next.next.next.next = new Node(5);
		linkedList.head.next.next.next.next.next = new Node(6);

		printLinkedList(linkedList.head);
		linkedList.head = reverseAlinkedList(linkedList.head);
		printLinkedList(linkedList.head);
	 linkedList.head = reverseALinkedListRecursively(linkedList.head,
		 null);
		 printLinkedList(linkedList.head);
		linkedList.head = reverseAlinkedLIstBy(linkedList.head, 2);

		printLinkedList(linkedList.head);
	}

	public static void printLinkedList(Node head) {

		while (head != null) {
			System.out.print(head.data);
			System.out.print("-");
			head = head.next;
		}
		System.out.println();

	}

	public static Node reverseAlinkedList(Node head) {

		Node current = head;
		Node previous = null;
		Node next = null;

		while (current != null) {
			next = current.next;
			current.next = previous;
			previous = current;
			current = next;

		}

		head = previous;
		return head;

	}

	public static Node reverseALinkedListRecursively(Node current, Node previous) {

		if (current.next == null) {

			head=current;
			current.next = previous;
			return null;
		}

		Node nextOne = current.next;
		current.next = previous;

		reverseALinkedListRecursively(nextOne, current);

		return head;

	}

	public static Node reverseAlinkedLIstBy(Node head, int k) {

		Node next = null;
		Node prev = null;
		Node current = head;

		int counter = 0;
		while (counter < k && current != null) {

			next = current.next;
			current.next = prev;
			prev = current;
			current = next;
			counter++;

		}

		if (next != null) {

			head.next = reverseAlinkedLIstBy(next, k);
		}

		return prev;

	}
	
	

}
