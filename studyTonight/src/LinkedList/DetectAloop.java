package LinkedList;

public class DetectAloop {

	
	public static boolean detectLoop(Node head){
		
		Node slow=head;
		Node fast=head;
		
		while(slow != null && fast != null && fast.next!=null){
			
			if(slow.data==fast.data){
				
				return true;
			}
			
			slow=slow.next;
			fast=fast.next;
		}
		
		return false;
	}
}
