package ArraysAndMatrix;

public class IslandProblemTest {

	public static final int[] offsets = { -1, 0, +1 };

	public static void main(String args[]) {

		int[][] multi = new int[][] { { 1, 1, 0, 0 }, { 0, 1, 0, 0 }, { 1, 0, 0, 1 }, { 0, 0, 0, 0 },
				{ 1, 0, 1, 0 } };

		System.out.println(getIslandCount(multi));
	}

	public static int getIslandCount(int[][] matrix) {

		boolean[][] visited = new boolean[matrix.length][matrix[0].length];
		System.out.println(matrix.length);
		System.out.println(matrix[0].length);

		int counter = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {

				if (matrix[i][j] == 1 && visited[i][j] == false) {

					counter = counter + 1;
					doDFs(matrix, i, j, visited);
				}

			}
		}

		return counter;
	}

	public static void doDFs(int[][] matrix, int i, int j, boolean[][] visited) {

		if (visited[i][j]) {

			return;
		}
		visited[i][j] = true;
		int x, y;
		for (int l = 0; l < offsets.length; l++) {
			x = offsets[l];
			for (int m = 0; m < offsets.length; m++) {
				y = offsets[m];

				if (x == 0 && y == 0) {
					continue;
				}
				if (visitedChecker(matrix, i + x, j + y)) {

					doDFs(matrix, i + x, j + y, visited);
				}
			}
		}

	}

	public static boolean visitedChecker(int[][] matrix, int i, int j) {

		if (i < matrix.length && j < matrix[0].length && i >= 0 && j >= 0) {

			if (matrix[i][j] == 1) {
				return true;
			}
		}

		return false;
	}
}
