package ArraysAndMatrix;

public class CountIslandProblem {

	final static int[] offsets = { -1, 0, +1 };

	public static void main(String args[]) {

		int[][] multi = new int[][] { {1, 1, 0, 0, 0},
                                      {0, 1, 0, 0, 1},
                                      {1, 0, 0, 1, 1},
                                      {0, 0, 0, 0, 0},
                                      {1, 0, 1, 0, 1} };

				System.out.println(countIslands(multi));
	}

	public static int countIslands(int[][] matrix) {

		int counter = 0;
		boolean[][] checker = new boolean[matrix.length][matrix[0].length];

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] == 1 && checker[i][j] == false) {
					counter = counter + 1;
					doDFS(matrix, i, j, checker);
				}

			}
		}
		
		return counter;

	}

	private static boolean neighborExists(int[][] matrix, int i, int j) {
		if ((i >= 0) && (i < matrix.length) && (j >= 0) && (j < matrix[0].length)) {
			if (matrix[i][j] == 1) {
				return true;
			}
		}

		return false;
	}

	private static void doDFS(int[][] matrix, int i, int j, boolean[][] visited) {
		if (visited[i][j]) {
			return;
		}

		visited[i][j] = true;

		int xOffset, yOffset;
		for (int l = 0; l < offsets.length; l++) {
			xOffset = offsets[l];
			for (int m = 0; m < offsets.length; m++) {
				yOffset = offsets[m];

				if (xOffset == 0 && yOffset == 0) {
					continue;
				}

				if (neighborExists(matrix, i + xOffset, j + yOffset)) {
					doDFS(matrix, i + xOffset, j + yOffset, visited);
				}
			}
		}
	}

}
