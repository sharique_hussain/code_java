package SortingAlgorithms;

public class RecursiveMergeSort {

	public static void main(String args[]) {

		int[] array = { 12, 11, 13, 5, 6, 7 };

		for (int i = 0; i < array.length; i++) {

			System.out.print(array[i]+" ");
		}
		System.out.println();
		sort(array, 0, array.length);

		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
	}

	public static void merge(int array[], int l, int m, int r) {

		int n1 = m - l + 1;
		int n2 = r - m;

		int[] left = new int[n1];
		int[] right = new int[n2];

		int index = 0;
		for (int i = 0; i < n1; i++) {

			left[i] = array[index];
		}
		for (int j = 0; j < n2; j++) {

			right[j] = array[index];
		}

		int i = 0, j = 0, k = 0;

		while (i < n1 && j < n2) {
			if (left[i] < right[j]) {
				array[k] = left[i];
				k++;
				i++;
			} else {
				array[k] = right[j];
				j++;
				k++;

			}

		}

		while (i < n1) {

			array[k] = left[i];
			i++;
			k++;

		}

		while (j < n2) {

			array[k] = left[j];
			j++;
			k++;

		}

	}

	public static void sort(int[] array, int l, int r) {

		if (l < r) {

			int middle = (l + r) / 2;

			sort(array, l, middle);

			sort(array, middle + 1, r);

			merge(array, l, middle, r);

		}

	}
}
