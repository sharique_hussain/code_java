package SortingAlgorithms;

import java.util.Arrays;
import java.util.Collections;

public class MergeSort {

	public static void main(String args[]){
		
		
		int[] array={12,11,13,5,6,7};
		
		printAray(array);
		sort(array,0,array.length-1);
		
		printAray(array);
		
	//	Arrays.sort(array);
	
		
	}
	
	public static void sort(int[] array,int l,int r){
		
		if(l<r){
			
			int middle=(l+r)/2;
			
			sort(array,l,middle);
			
			sort(array,middle+1,r);
			
			merge(array,l,middle,r);
		}
	}
	
	public static void merge(int[] array,int l,int m, int r){
		
		int n1=m-l+1;
		int n2=r-m;
		
		int[] array1=new int[n1];
		
		int[] array2=new int[n2];
		
		//copy values in two arrays
		
		for(int i=0;i<n1;i++){
			array1[i]=array[l+i];
		}
		
		for(int i=0;i<n2;i++){
			array2[i]=array[m+1+i];
		}
		
		int i=0,j=0;
		int k=l;
		
		while(i<n1 && j<n2){
			
			if(array1[i]<=array2[j]){
				
				array[k]=array1[i];
				i++;
			}
			else{
				array[k]=array2[j];
				j++;
			}
			k++;
		}
		
		while(i<n1){
			array[k]=array1[i];
			i++;
			k++;
		}
		
		while(j<n2){
			array[k]=array2[j];
			j++;
			k++;
		}
	}
	
	public static void printAray(int[] array){
		
		for(int i=0;i<array.length;i++){
			
			System.out.println(" "+array[i]);
		}
	}
}
