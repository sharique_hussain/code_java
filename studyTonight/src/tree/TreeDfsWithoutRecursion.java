package tree;

import java.util.Stack;

public class TreeDfsWithoutRecursion {

	Nodes root;

	public static void main(String args[]) {

		TreeDfsWithoutRecursion tree = new TreeDfsWithoutRecursion();
		tree.root = new Nodes(1);
		tree.root.left = new Nodes(2);
		tree.root.right = new Nodes(3);
		tree.root.left.left = new Nodes(4);
		tree.root.left.right = new Nodes(5);
		
		printInorderTraversal(tree.root);

	}

	public static void printInorderTraversal(Nodes node) {

		Stack<Nodes> newStack = new Stack<Nodes>();

		Nodes check = node;
		while (check != null) {

			newStack.add(check);
			check = check.left;
		}

		while (newStack.size() > 0) {

			check = newStack.pop();
			System.out.println(check.data);
			if (check.right != null) {

				check = check.right;

				while (check != null) {

					newStack.push(check);
					check = check.left;

				}
			}

		}

	}
}
