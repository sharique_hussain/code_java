package tree;

import java.util.ArrayList;
import java.util.List;

public class AddingIntegersOfAListUsingRecursion {

	public static void main(String args[]) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(6);
		list.add(7);
		list.add(3);

		System.out.println(recursionSum(list));
	}

	public static int recursionSum(List<Integer> list) {

		if (list.size() == 0) {
			return 0;
		}

		return list.get(0) + recursionSum((list.subList(1, list.size())));

	}
}
