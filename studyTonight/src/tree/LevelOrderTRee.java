package tree;

import java.util.LinkedList;
import java.util.Queue;

public class LevelOrderTRee {

	Nodes root;

	public static void main(String args[]) {

		LevelOrderTRee tree = new LevelOrderTRee();
		tree.root = new Nodes(1);
		tree.root.left = new Nodes(2);
		tree.root.right = new Nodes(3);
		tree.root.left.left = new Nodes(4);
		tree.root.left.right = new Nodes(5);

		System.out.println("Level order traversal of binary tree is ");

		int height = getHeightOfTree(tree.root);

		for (int i = 1; i <= height; i++) {

			printLevelOrderRecursively(tree.root, i);

		}

		System.out.println("Level order traversal of binary tree is  Using Queue");

		printLevelOrderTraversalUsingQueue(tree.root);
	}

	public static void printLevelOrderTraversalUsingQueue(Nodes head) {

		if (head != null) {

			Queue<Nodes> queue = new LinkedList<Nodes>();

			queue.add(head);

			while (!queue.isEmpty()) {
				Nodes node = queue.poll();

				System.out.println(node.data);

				if (node.left != null) {

					queue.add(node.left);

				}
				if (node.right != null) {

					queue.add(node.right);

				}

			}

		}

	}

	public static void printLevelOrderRecursively(Nodes head, int level) {

		if (head == null) {

			return;
		}

		if (level == 1) {

			System.out.println(head.data);

		} else if (level > 1) {

			printLevelOrderRecursively(head.left, level - 1);

			printLevelOrderRecursively(head.right, level - 1);

		}

	}

	public static int getHeightOfTree(Nodes head) {

		if (head == null) {

			return 0;
		}

		int leftHeight = getHeightOfTree(head.left);

		int rightHeight = getHeightOfTree(head.right);

		if (leftHeight > rightHeight) {

			return leftHeight + 1;
		} else {
			return rightHeight + 1;
		}

	}

	public static int getHeight(Node head) {

		if (head == null) {

			return 0;
		}

		int lheight = getHeight(head.left);

		int rheight = getHeight(head.right);

		if (lheight > rheight) {

			return lheight + 1;
		} else {
			return rheight + 1;
		}
	}

	public static void traverse(Node head, int level) {

		if (head == null) {

			return;
		}
		if (level == 1) {

			System.out.println(head.data);
		} else {

			traverse(head.left, level - 1);

			traverse(head.right, level - 1);

		}
	}

}
