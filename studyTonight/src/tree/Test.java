
public class Test {


	public CarSearchResult getAllCarsSearchResult(SearchRequest request, String cityId, boolean specialWeekendEnable,
			HttpServletRequest httpRequest) throws Exception {
		request.setCity(cityId);


		
		String uri = httpRequest.getRequestURI();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		City city = FECache.getCityByCityId(request.getCity());
		for (Cars car : result.getAvailableCars()) {
			
			if (uri != null && !uri.isEmpty() && uri.contains("/api/v2/search/post")) {
				if (city.isCouponEnable()) {
					/*PromoResponse autoAPpliedCoupon = promoService.getAutoAppliedPromo(
							sdf.parse(request.getStartDate()), sdf.parse(request.getEndDate()),
							car.getTotalPriceForSearchPage());

					if (autoAPpliedCoupon != null) {*/
			Date endDate = null;
			
			PromoResponse autoAPpliedCoupon = null;
			if (car.isMinimumPricingApplied()) {
				Date startDate = sdf.parse(request.getStartDate());
				endDate = BmsUtils.addMinutes(startDate, (int) car.getMinimumPricingDuration());
				PromoResponse autoAppliedCouponWithModifiedEndDate = promoService.getAutoAppliedPromo(sdf.parse(request.getStartDate()),
						sdf.parse(request.getEndDate()), car.getTotalPriceForSearchPage(), car.getPerHourPriceBreakUp(), city.getId());
				
				PromoResponse autoAppliedCouponOriginalEndDate = promoService.getAutoAppliedPromo(sdf.parse(request.getStartDate()),
						endDate, car.getTotalPriceForSearchPage(), car.getPerHourPriceBreakUp(), city.getId());
				
				if(autoAppliedCouponWithModifiedEndDate == null) {
					autoAPpliedCoupon = autoAppliedCouponOriginalEndDate;
				} else if(autoAppliedCouponOriginalEndDate == null) {
					autoAPpliedCoupon = autoAppliedCouponWithModifiedEndDate;
				} else {
					autoAPpliedCoupon = autoAppliedCouponWithModifiedEndDate.getWeight() > autoAppliedCouponOriginalEndDate.getWeight() ?
							autoAppliedCouponWithModifiedEndDate : autoAppliedCouponOriginalEndDate;
				}
				car.setPromoRecommendationAvailable(false);
			} else {
				endDate = sdf.parse(request.getEndDate());
				autoAPpliedCoupon = promoService.getAutoAppliedPromo(sdf.parse(request.getStartDate()),
						endDate, car.getTotalPriceForSearchPage(), car.getPerHourPriceBreakUp(), city.getId());
			}
			

			if (autoAPpliedCoupon != null) {
				car.setPromoRecommendationAvailable(false);
				car.setAutoAppliedPromoEnglish(autoAPpliedCoupon.getAutoAppliedCouponEnglish());
				if (autoAPpliedCoupon.getPromoValue() != null) {
					int totalAmt = car.getTotalPriceForSearchPage();
					int promoAmt = (int) Math.floor(Float.parseFloat(autoAPpliedCoupon.getPromoValue()));
					if (autoAPpliedCoupon.getPromoMaxValue() != null) {
						int maxPromoValue = Integer.parseInt(autoAPpliedCoupon.getPromoMaxValue());
						if (maxPromoValue > 0 && promoAmt > maxPromoValue) {
							promoAmt = maxPromoValue;
						}
					}
				car.setAutoAppliedPromo(autoAPpliedCoupon.getName());
				car.setTotalPriceForSearchPageAfterPointRedemption(
						totalAmt - promoAmt - car.getMaxApplicablePoints());
				car.setMaxAutoCouponApplied(promoAmt);
				car.setAmountForCashback(car.getAmountForCashback() + autoAPpliedCoupon.getvMiles());
					}

				}
			}
			} else {
				
				Date bookingEndDate=null;
				if (car.isMinimumPricingApplied()) {
					Date startDate = sdf.parse(request.getStartDate());
					bookingEndDate = BmsUtils.addMinutes(startDate, (int) car.getMinimumPricingDuration());
				} else {
					bookingEndDate = sdf.parse(request.getEndDate());
				}
				
				PromoResponse autoAPpliedCoupon = promoService.getAutoAppliedPromo(sdf.parse(request.getStartDate()),
						bookingEndDate, car.getTotalPriceForSearchPage(), car.getPerHourPriceBreakUp(), city.getId());

				if (autoAPpliedCoupon != null) {
					car.setPromoRecommendationAvailable(false);
					car.setAutoAppliedPromoEnglish(autoAPpliedCoupon.getAutoAppliedCouponEnglish());
					if (autoAPpliedCoupon.getPromoValue() != null) {
						int totalAmt = car.getTotalPriceForSearchPage();
						int promoAmt = (int) Math.floor(Float.parseFloat(autoAPpliedCoupon.getPromoValue()));

						if (autoAPpliedCoupon.getPromoMaxValue() != null) {
							int maxPromoValue = Integer.parseInt(autoAPpliedCoupon.getPromoMaxValue());

							if (maxPromoValue > 0 && promoAmt > maxPromoValue) {
								promoAmt = maxPromoValue;
							}
						}
						car.setAutoAppliedPromo(autoAPpliedCoupon.getName());
					car.setTotalPriceForSearchPageAfterPointRedemption(
							totalAmt - promoAmt - car.getMaxApplicablePoints());
					car.setMaxAutoCouponApplied(promoAmt);

					if (car.getvFlexiInstalments() != null) {

						updateInstalmentsAsPerCouponApplied(car.getvFlexiInstalments(), promoAmt,
								car.getMaxApplicablePoints());
					}

					car.setAmountForCashback(car.getAmountForCashback() + autoAPpliedCoupon.getvMiles());
					}
				}
			}
			car.setRecommendationAvailable(checkForRecommendations(car));
		}

		if (result.getPartialyAvailableCars() != null && !result.getPartialyAvailableCars().isEmpty()) {
			for (Cars car : result.getPartialyAvailableCars()) {
				List<AvailabilitySlotesModel> avList = new ArrayList<>();
				for (AvailabilitySlotesModel availabilitySlotesModels : car.getAvailabilitySlotesModels()) {

					if (uri != null && !uri.isEmpty() && uri.contains("/api/v2/search/post")) {
						if (city.isCouponEnable()) {
							PromoResponse autoAPpliedCoupon = promoService.getAutoAppliedPromo(
									availabilitySlotesModels.getStartDate(), availabilitySlotesModels.getEndDate(),
									car.getTotalPriceForSearchPage(), car.getPerHourPriceBreakUp(), city.getId());

							if (autoAPpliedCoupon != null) {
								car.setPromoRecommendationAvailable(false);
								availabilitySlotesModels
										.setAutoAppliedPromoEnglish(autoAPpliedCoupon.getAutoAppliedCouponEnglish());
								if (autoAPpliedCoupon.getPromoValue() != null) {
									int totalAmt = car.getTotalPriceForSearchPage();
									int promoAmt = (int) Math
											.floor(Float.parseFloat(autoAPpliedCoupon.getPromoValue()));

									if (autoAPpliedCoupon.getPromoMaxValue() != null) {
										int maxPromoValue = Integer.parseInt(autoAPpliedCoupon.getPromoMaxValue());

										if (maxPromoValue > 0 && promoAmt > maxPromoValue) {
											promoAmt = maxPromoValue;
										}
									}

									availabilitySlotesModels.setAutoAppliedPromo(autoAPpliedCoupon.getName());
									availabilitySlotesModels.setTotalPriceForSearchPageAfterPointRedemption(
											totalAmt - promoAmt - car.getMaxApplicablePoints());
									availabilitySlotesModels.setMaxAutoCouponApplied(promoAmt);
								}
							}
						}
					} else {
						PromoResponse autoAPpliedCoupon = promoService.getAutoAppliedPromo(
								availabilitySlotesModels.getStartDate(), availabilitySlotesModels.getEndDate(),
								car.getTotalPriceForSearchPage(), car.getPerHourPriceBreakUp(), city.getId());

						if (autoAPpliedCoupon != null) {
							car.setPromoRecommendationAvailable(false);
							availabilitySlotesModels
									.setAutoAppliedPromoEnglish(autoAPpliedCoupon.getAutoAppliedCouponEnglish());
							if (autoAPpliedCoupon.getPromoValue() != null) {
								int totalAmt = car.getTotalPriceForSearchPage();
								int promoAmt = (int) Math.floor(Float.parseFloat(autoAPpliedCoupon.getPromoValue()));

								if (autoAPpliedCoupon.getPromoMaxValue() != null) {
									int maxPromoValue = Integer.parseInt(autoAPpliedCoupon.getPromoMaxValue());

									if (maxPromoValue > 0 && promoAmt > maxPromoValue) {
										promoAmt = maxPromoValue;
									}
								}
								availabilitySlotesModels.setAutoAppliedPromo(autoAPpliedCoupon.getName());
								availabilitySlotesModels.setTotalPriceForSearchPageAfterPointRedemption(
										totalAmt - promoAmt - car.getMaxApplicablePoints());
								availabilitySlotesModels.setMaxAutoCouponApplied(promoAmt);
							}
						}
					}
					availabilitySlotesModels.setTotalPriceForSearchPage(car.getTotalPriceForSearchPage());
					avList.add(availabilitySlotesModels);
				}
				if (avList != null && !avList.isEmpty()) {
					car.setAvailabilitySlotesModels(avList);
				}
				car.setRecommendationAvailable(checkForRecommendations(car));
			}
		}

		User user = VolerUtils.getLoggedInUser(httpRequest);
		if (user != null) {

			if (result.getAvailableCars() != null) {
				List<Cars> availableCars = result.getAvailableCars();
				for (Cars car : availableCars) {
					int maxApplicableAmt = 0;

					if (car.getTotalPriceForSearchPageAfterPointRedemption() > 0) {
						maxApplicableAmt = umsPointService.getMaxPointsApplicable(user.getObjectId(),
								car.getTotalPriceForSearchPageAfterPointRedemption());
					} else {
						maxApplicableAmt = umsPointService.getMaxPointsApplicable(user.getObjectId(),
								car.getTotalPriceForSearchPage());
					}

					int totalAmt = car.getTotalPriceForSearchPage();
					car.setMaxApplicablePoints(maxApplicableAmt);
					car.setTotalPriceForSearchPageAfterPointRedemption(
							totalAmt - maxApplicableAmt - car.getMaxAutoCouponApplied());
					if (car.getvFlexiInstalments() != null) {

						int vmilesForVflexi = umsPointService.getMaxPointsApplicable(user.getObjectId(),
								(int) car.getvFlexiInstalments().getFirstVflexiInstalment());
						updateVmiles(car.getvFlexiInstalments(), vmilesForVflexi);
					}

				}
			}

			if (result.getPartialyAvailableCars() != null && !result.getPartialyAvailableCars().isEmpty()) {
				List<Cars> partialAvailableCars = result.getPartialyAvailableCars();
				for (Cars car : partialAvailableCars) {
					List<AvailabilitySlotesModel> avList = new ArrayList<>();
					for (AvailabilitySlotesModel availabilitySlotesModels : car.getAvailabilitySlotesModels()) {
						int maxApplicableAmt = 0;

						if (availabilitySlotesModels.getTotalPriceForSearchPageAfterPointRedemption() > 0) {
							maxApplicableAmt = umsPointService.getMaxPointsApplicable(user.getObjectId(),
									availabilitySlotesModels.getTotalPriceForSearchPageAfterPointRedemption());
						} else {
							maxApplicableAmt = umsPointService.getMaxPointsApplicable(user.getObjectId(),
									car.getTotalPriceForSearchPage());
						}

						int totalAmt = Math.round(availabilitySlotesModels.getCarPricingResponseModel().getPrice());
						availabilitySlotesModels.setMaxApplicablePoints(maxApplicableAmt);
						availabilitySlotesModels.setTotalPriceForSearchPageAfterPointRedemption(
								totalAmt - maxApplicableAmt - availabilitySlotesModels.getMaxAutoCouponApplied());
						availabilitySlotesModels.setTotalPriceForSearchPage(totalAmt);
						avList.add(availabilitySlotesModels);
					}
					if (avList != null && !avList.isEmpty()) {
						car.setAvailabilitySlotesModels(avList);
					}
				}
			}

			if (result.getUnavailableCars() != null) {
				List<Cars> unavailableCars = result.getUnavailableCars();
				for (Cars car : unavailableCars) {

					int maxApplicableAmt = umsPointService.getMaxPointsApplicable(user.getObjectId(),
							car.getTotalPriceForSearchPage());
					int totalAmt = car.getTotalPriceForSearchPage();
					car.setMaxApplicablePoints(maxApplicableAmt);
					car.setTotalPriceForSearchPageAfterPointRedemption(totalAmt - maxApplicableAmt);
				}
			}
		}

		boolean checkForFreeDelivery = false;
		
		VendorMHBResponse vendorMHBResponse = ipmsService.getMHBForCity(cityId);
		VendorTatResponse vendorTatResponse = ipmsService.getTATForCity(cityId);
		
		int maxMonth = commonComponent.getVisibleCalendarMonths();

		for (Cars car : result.getAvailableCars()) {

			setPriceBreakup(car);

// ModelConverter

			if (car.getvFlexiInstalments() != null && car.getvFlexiInstalments().isvFlexiEnabled()) {

				car.setFlexiInstallmentModel(
						ModelConverter.convertForVflexiInstallmentsModel(car.getvFlexiInstalments()));
			}

			// Checking For Free Delivery

			boolean freeDelivery = checkForFreeDelivery(sdf.parse(request.getStartDate()),
					sdf.parse(request.getEndDate()));

			if (freeDelivery)
				car.setFreeDelivery(true);


			if(car.isPromoRecommendationEndVarient()){
				
				Date modifiedEndDate = GetTimeDifferenceUtils.modifyDate(sdf.parse(request.getEndDate()),
						(int) car.getAddTimeForApplyPromo());

				DateValidationRequest validationRequest = new DateValidationRequest();
				validationRequest.setStartDate(request.getStartDate());
				validationRequest.setEndDate(sdf.format(modifiedEndDate));
				validationRequest.setCityId(cityId);
				validationRequest.setMhb(vendorMHBResponse);
				validationRequest.setTat(vendorTatResponse);
				validationRequest.setMinMonthsVisibility(maxMonth);
				String messageForEndDateChanges = DateTimeValidation.validateDateTime(validationRequest);

				if (messageForEndDateChanges == null) {

					HoursAndMinsBreakup hoursAndMinsBreakup = getHoursAndMins(car.getAddTimeForApplyPromo());
					String msg = null;

					if (hoursAndMinsBreakup != null) {

						if (hoursAndMinsBreakup.getHours() > 0 && hoursAndMinsBreakup.getMins() <= 0) {

							msg = "" + hoursAndMinsBreakup.getHours() + " Hour(s)";
						} else if (hoursAndMinsBreakup.getHours() > 0 && hoursAndMinsBreakup.getMins() > 0) {

							msg = "" + hoursAndMinsBreakup.getHours() + " Hour(s) and " + hoursAndMinsBreakup.getMins()
									+ " mins";
						} else {

							msg = "" + hoursAndMinsBreakup.getMins() + " mins";
						}
					}

					FreeDeliveryDetails freeDeliveryDetails = new FreeDeliveryDetails();
					freeDeliveryDetails.setMsg(msg);
					freeDeliveryDetails.setStartDate(request.getStartDate());
					freeDeliveryDetails.setEndDate(sdf.format(modifiedEndDate));

					car.setPromoRecommedationDetails(freeDeliveryDetails);
				}

			} else if (car.isPromoRecommendationStartVarient()) {

				Date modifiedStartDate = GetTimeDifferenceUtils.modifyDate(sdf.parse(request.getStartDate()),
						-(int) car.getAddTimeForApplyPromo());

				DateValidationRequest validationRequest = new DateValidationRequest();
				validationRequest.setStartDate(sdf.format(modifiedStartDate));
				validationRequest.setEndDate(request.getEndDate());
				validationRequest.setCityId(cityId);
				validationRequest.setMhb(vendorMHBResponse);
				validationRequest.setTat(vendorTatResponse);
				validationRequest.setMinMonthsVisibility(maxMonth);
				String messageForStartDateChanges = DateTimeValidation.validateDateTime(validationRequest);

				if (messageForStartDateChanges == null) {

					HoursAndMinsBreakup hoursAndMinsBreakup = getHoursAndMins(car.getAddTimeForApplyPromo());
					String msg = null;

					if (hoursAndMinsBreakup != null) {

						if (hoursAndMinsBreakup.getHours() > 0 && hoursAndMinsBreakup.getMins() <= 0) {

							msg = "" + hoursAndMinsBreakup.getHours() + " Hour(s)";
						} else if (hoursAndMinsBreakup.getHours() > 0 && hoursAndMinsBreakup.getMins() > 0) {

							msg = "" + hoursAndMinsBreakup.getHours() + " Hour(s) and " + hoursAndMinsBreakup.getMins()
									+ " mins";
						} else {

							msg = "" + hoursAndMinsBreakup.getMins() + " mins";
						}
					}

					FreeDeliveryDetails freeDeliveryDetails = new FreeDeliveryDetails();
					freeDeliveryDetails.setMsg(msg);
					freeDeliveryDetails.setEndDate(request.getEndDate());
					freeDeliveryDetails.setStartDate(sdf.format(modifiedStartDate));

					car.setPromoRecommedationDetails(freeDeliveryDetails);
				}
			}

			if (car.isFreeDeliveryEndVarient()) {

				Date modifiedEndDate = GetTimeDifferenceUtils.modifyDate(sdf.parse(request.getEndDate()),
						(int) car.getAddTimeForFreeDelivery());

				DateValidationRequest validationRequest = new DateValidationRequest();
				validationRequest.setStartDate(request.getStartDate());
				validationRequest.setEndDate(sdf.format(modifiedEndDate));
				validationRequest.setCityId(cityId);
				validationRequest.setMhb(vendorMHBResponse);
				validationRequest.setTat(vendorTatResponse);
				validationRequest.setMinMonthsVisibility(maxMonth);
				String messageForEndDateChanges = DateTimeValidation.validateDateTime(validationRequest);

				if (messageForEndDateChanges == null) {

					car.setFreeDelivery(true);
					HoursAndMinsBreakup hoursAndMinsBreakup = getHoursAndMins(car.getAddTimeForFreeDelivery());
					String msg = null;

					if (hoursAndMinsBreakup != null) {

						if (hoursAndMinsBreakup.getHours() > 0 && hoursAndMinsBreakup.getMins() <= 0) {

							msg = "" + hoursAndMinsBreakup.getHours() + " Hour(s)";
						} else if (hoursAndMinsBreakup.getHours() > 0 && hoursAndMinsBreakup.getMins() > 0) {

							msg = "" + hoursAndMinsBreakup.getHours() + " Hour(s) and " + hoursAndMinsBreakup.getMins()
									+ " mins";
						} else {

							msg = "" + hoursAndMinsBreakup.getMins() + " mins";
						}
					}

					FreeDeliveryDetails freeDeliveryDetails = new FreeDeliveryDetails();
					freeDeliveryDetails.setMsg(msg);
					freeDeliveryDetails.setStartDate(request.getStartDate());
					freeDeliveryDetails.setEndDate(sdf.format(modifiedEndDate));

					car.setDeliveryDetails(freeDeliveryDetails);
				}

			} else if (car.isFreeDeliveryStartVarient()) {

				Date modifiedStartDate = GetTimeDifferenceUtils.modifyDate(sdf.parse(request.getStartDate()),
						-(int) car.getAddTimeForFreeDelivery());

				DateValidationRequest validationRequest = new DateValidationRequest();
				validationRequest.setStartDate(sdf.format(modifiedStartDate));
				validationRequest.setEndDate(request.getEndDate());
				validationRequest.setCityId(cityId);
				validationRequest.setMhb(vendorMHBResponse);
				validationRequest.setTat(vendorTatResponse);
				validationRequest.setMinMonthsVisibility(maxMonth);
				String messageForStartDateChanges = DateTimeValidation.validateDateTime(validationRequest);

				if (messageForStartDateChanges == null) {

					car.setFreeDelivery(true);
					HoursAndMinsBreakup hoursAndMinsBreakup = getHoursAndMins(car.getAddTimeForFreeDelivery());
					String msg = null;

					if (hoursAndMinsBreakup != null) {

						if (hoursAndMinsBreakup.getHours() > 0 && hoursAndMinsBreakup.getMins() <= 0) {

							msg = "" + hoursAndMinsBreakup.getHours() + " Hour(s)";
						} else if (hoursAndMinsBreakup.getHours() > 0 && hoursAndMinsBreakup.getMins() > 0) {

							msg = "" + hoursAndMinsBreakup.getHours() + " Hour(s) and " + hoursAndMinsBreakup.getMins()
									+ " mins";
						} else {

							msg = "" + hoursAndMinsBreakup.getMins() + " mins";
						}
					}

					FreeDeliveryDetails freeDeliveryDetails = new FreeDeliveryDetails();
					freeDeliveryDetails.setMsg(msg);
					freeDeliveryDetails.setEndDate(request.getEndDate());
					freeDeliveryDetails.setStartDate(sdf.format(modifiedStartDate));

					car.setDeliveryDetails(freeDeliveryDetails);
				}
			}

			if (car.isZeroSdEndVarient()) {

				Calendar cal = new GregorianCalendar();
				Date endDate = sdf.parse(request.getEndDate());
				cal.setTime(endDate);
				cal.add(Calendar.MINUTE, (int) car.getMinutesToAddForZeroSD());
				Date newEndDate = cal.getTime();

				DateValidationRequest validationRequest = new DateValidationRequest();
				validationRequest.setEndDate(sdf.format(newEndDate));
				validationRequest.setStartDate(request.getStartDate());
				validationRequest.setCityId(cityId);
				validationRequest.setMhb(ipmsService.getMHBForCity(cityId));
				validationRequest.setTat(ipmsService.getTATForCity(cityId));
				validationRequest.setMinMonthsVisibility(maxMonth);
				
				String message = DateTimeValidation.validateDateTime(validationRequest);

				if (message == null) {
					car.setNewStartDateTimeForZeroSD(request.getStartDate());
					car.setNewEndDateTimeForZeroSD(sdf.format(newEndDate));

					String duration = "";
					int addHours = (int) (car.getMinutesToAddForZeroSD() / 60);
					int addMins = (int) car.getMinutesToAddForZeroSD() % 60;
					if (addHours > 0) {
						duration += addHours + " Hour(s) ";
					}
					if (addMins > 0) {
						duration += " 30 Mins";
					}
					car.setZeroSdDurationString(duration);
				}

			} else if (car.isZeroSdStartVarient()) {

				Calendar cal = new GregorianCalendar();
				Date startDate = sdf.parse(request.getStartDate());
				cal.setTime(startDate);
				cal.add(Calendar.MINUTE, -(int) car.getMinutesToAddForZeroSD());
				Date newStartDate = cal.getTime();

				DateValidationRequest validationRequest = new DateValidationRequest();
				validationRequest.setEndDate(request.getEndDate());
				validationRequest.setStartDate(sdf.format(newStartDate));
				validationRequest.setCityId(cityId);
				validationRequest.setMhb(vendorMHBResponse);
				validationRequest.setTat(vendorTatResponse);
				validationRequest.setMinMonthsVisibility(maxMonth);

				String message = DateTimeValidation.validateDateTime(validationRequest);

				if (message == null) {
					car.setNewStartDateTimeForZeroSD((sdf.format(newStartDate)));
					car.setNewEndDateTimeForZeroSD(request.getEndDate());
					String duration = "";
					int addHours = (int) (car.getMinutesToAddForZeroSD() / 60);
					int addMins = (int) car.getMinutesToAddForZeroSD() % 60;
					if (addHours > 0) {
						duration += addHours + " Hour(s) ";
					}
					if (addMins > 0) {
						duration += " 30 Mins";
					}
					car.setZeroSdDurationString(duration);
				}

			}
			car.setRecommendationAvailable(checkForRecommendations(car));
		}

		for (Cars car : result.getUnavailableCars()) {

			setPriceBreakup(car);
		}
		return result;
	}

	private boolean checkForRecommendations(Cars car) {
		if (car.getNewStartDateTimeForZeroSD() != null || car.getTotalDealsAvailable() > 0
				|| car.isFreeDelivery() || car.isPromoRecommendationAvailable()) {
			return true;
		}
		return false;
	}

	private PromoRecommendation getDetailsForPromoPromotion(SearchRequest request, String cityId)
			throws ParseException, java.text.ParseException, ExecutionException {

		Date startDate = DateTimeUtils.getDateFromStr(request.getStartDate(), "dd-MM-yyyy HH:mm");
		Date endDate = DateTimeUtils.getDateFromStr(request.getEndDate(), "dd-MM-yyyy HH:mm");
		long addTimeForPromo = 0;
		String promoCode = "";
		String promoValue = "";

		int minTimeForPromoInMins = 0;
		int addTimeInt = 1440;
		String addTime = FECache.getAllProperties().get("add_time_for_promo_recommendation_in_minutes");

		if (addTime != null && !addTime.isEmpty()) {
			addTimeInt = Integer.parseInt(addTime);
		}

		PromoResponse promoResponse1 = promoService.getAutoAppliedPromoDetails(startDate, endDate, cityId);

		if (promoResponse1 != null) {

			Date modifiedEndDate = GetTimeDifferenceUtils.modifyDate(endDate, addTimeInt);
			PromoResponse promoResponseForModifiedDate = promoService.getAutoAppliedPromoDetails(startDate,
					modifiedEndDate, cityId);

			if (promoResponseForModifiedDate != null) {

				if (!promoResponse1.getName().equalsIgnoreCase(promoResponseForModifiedDate.getName())) {

					minTimeForPromoInMins = promoResponseForModifiedDate.getMinLimitOfDurationInMins();
					promoCode = promoResponseForModifiedDate.getName();
					promoValue = promoResponseForModifiedDate.getPromoValue();
				}
			}

		} else {

			Date modifiedEndDate = GetTimeDifferenceUtils.modifyDate(endDate, addTimeInt);
			PromoResponse promoResponseForModifiedDate = promoService.getAutoAppliedPromoDetails(startDate,
					modifiedEndDate, cityId);

			if (promoResponseForModifiedDate != null) {
				minTimeForPromoInMins = promoResponseForModifiedDate.getMinLimitOfDurationInMins();
				promoCode = promoResponseForModifiedDate.getName();
				promoValue = promoResponseForModifiedDate.getPromoValue();
			}
		}

		PromoRecommendation promoRecommendation = null;
		if (minTimeForPromoInMins > 0) {

			addTimeForPromo = getTimeForWhichPromoWillBeApplied(startDate, endDate, minTimeForPromoInMins);

			promoRecommendation = new PromoRecommendation();
			promoRecommendation.setAddTimeForPromo(addTimeForPromo);
			promoRecommendation.setPromoCode(promoCode);
			promoRecommendation.setPromoValue(promoValue);
		}

		return promoRecommendation;
	}

	private long getTimeForWhichPromoWillBeApplied(Date startDate, Date endDate, int minTimeForPromoInMins) {

		long searchDuration = GetTimeDifferenceUtils.getTimeDifferenceInMinutes(startDate, endDate);

		long addTimeForPromo = 0;

		if (minTimeForPromoInMins > searchDuration) {

			addTimeForPromo = minTimeForPromoInMins - searchDuration;
		}

		return addTimeForPromo;
	}

	private HoursAndMinsBreakup getHoursAndMins(long diff) {

		HoursAndMinsBreakup hoursAndMinsBreakup = new HoursAndMinsBreakup();

		long hours = diff / 60;
		long mins = diff % 60;

		hoursAndMinsBreakup.setHours(hours);
		hoursAndMinsBreakup.setMins(mins);
		return hoursAndMinsBreakup;
	}

	private boolean checkForFreeDelivery(Date startDate, Date endDate) {

		long duration = endDate.getTime() - startDate.getTime();

		float totalHours = TimeUnit.MILLISECONDS.toHours(duration);

		if (totalHours > 48)
			return true;

		return false;
	}

	/**
	 * @param car
	 */
	public void setPriceBreakup(Cars car) {
		List<KeyValue> breakupList = new ArrayList<>();

		if (car.getPerHourPriceBreakUp() != null && car.getPerHourPricing() != null) {

			if (car.getPerHourPriceBreakUp().getWeekDaysHours() > 0) {

				String breakupLeft = "Weekday Tariff ["
						+ getBrakupString(car.getPerHourPriceBreakUp().getWeekDaysHours(),
								car.getPerHourPricing().getWeekDayHourPrice())
						+ "]";

				String breakupRight = String.valueOf(car.getPerHourPriceBreakUp().getWeekDaysHours()
						* car.getPerHourPricing().getWeekDayHourPrice());

				breakupList.add(new KeyValue(breakupLeft, breakupRight));
			}

			if (car.getPerHourPriceBreakUp().getWeekEndHours() > 0) {

				String breakupLeft = "Weekend Tariff ["
						+ getBrakupString(car.getPerHourPriceBreakUp().getWeekEndHours(),
								car.getPerHourPricing().getWeekEndHourPrice())
						+ "]";
				String breakupRight = String.valueOf(
						car.getPerHourPriceBreakUp().getWeekEndHours() * car.getPerHourPricing().getWeekEndHourPrice());

				breakupList.add(new KeyValue(breakupLeft, breakupRight));
			}

			if (car.getPerHourPriceBreakUp().getPeakHours() > 0) {

				String breakupLeft = "Peak Tariff [" + getBrakupString(car.getPerHourPriceBreakUp().getPeakHours(),
						car.getPerHourPricing().getPeakHourPrice()) + "]";
				String breakupRight = String.valueOf(
						car.getPerHourPriceBreakUp().getPeakHours() * car.getPerHourPricing().getPeakHourPrice());

				breakupList.add(new KeyValue(breakupLeft, breakupRight));
			}

		}

		car.setBreakupList(breakupList);
	}

	private String getBrakupString(double hours, long price) {

		double fractionalPart = hours - (int) hours;

		String breakupLeft = null;
		if (fractionalPart > 0) {
			breakupLeft = "" + hours + "hour(s) * <i class=\"fa fa-inr\"></i> " + price;

		} else {
			breakupLeft = "" + (int) hours + "hour(s) * <i class=\"fa fa-inr\"></i> " + price;
		}

		return breakupLeft;
	}

	public SummaryResponse getCarAvailabilityForSummary(SummaryRequest request, String cityId,
			HttpServletRequest httpReq, boolean genrateLead) throws Exception {

		SummaryResponse response = new SummaryResponse();
		request.setCity(cityId);
		CarSearchResult searchResult = checkCarAvailablity(request);

		if (searchResult != null) {
			if (searchResult.getAvailableCars() != null && searchResult.getAvailableCars().size() > 0) {
				TimeDifference diff = searchResult.getTimeDiffer();
				response.setDuration(diff);
				response.setStringTimeDuration(searchResult.getTimeForSearchPage());
				Cars car = null;
				for (Cars carObj : searchResult.getAvailableCars()) {
					car = carObj;
				}
				car = checkForZeroSDVariant(car, request, cityId);
				setPriceBreakup(car);
				response.setCar(car);
				response.setCancelable(car.isCancelable());
				Map<String, Accessories> allAccessories = null;
				try {
					allAccessories = FECache.getAllAccessories();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (allAccessories != null) {

					if (allAccessories.containsKey("Baby Seat")) {

						Number babySeatPrice = allAccessories.get("Baby Seat").getPrice();
						response.setBabySeatCharges(babySeatPrice.intValue());
						response.setTotalBabySeatPrice(calculateBabySeatPrice(babySeatPrice.intValue(), diff));

					}

					if (allAccessories.containsKey("Fuel Plan")) {
						response.setFuelPlanCharges(allAccessories.get("Fuel Plan").getPrice());
					}
				}

				City city = FECache.getCityByCityId(cityId);

				if (car.isVatInclusive()) {
					response.setVatPercentage("0");
				} else {
					response.setVatPercentage(city.getVat());
				}

				// response.setRefundableDeposit(String.valueOf(searchResult.getSecurityDeposit()));

				response.setPetrolPrice(String.valueOf(city.getPetrolPrice()));

				response.setDieselPrice(String.valueOf(city.getDieselPrice()));

				response.setFuelPriceOffset(String.valueOf(city.getFuelPriceOffset()));

				response.setStartTat(searchResult.getStartTat());
				response.setMinimumAllowTimeFromCurrentTimeInMinutes(
						searchResult.getMinimumAllowTimeFromCurrentTimeInMinutes());
				response.setEndTat(searchResult.getEndTat());
				response.setSpecialWeekendApplied(car.isCancelable());
				response.setCancellationSlabOne(searchResult.getCancellationSlabOne());
				response.setCancellationSlabTwo(searchResult.getCancellationSlabTwo());

				if (searchResult.isFreeDelivery()) {
					response.setDeliveryCharges(String.valueOf(0));

					response.setHoursAfterDelieveryChargesNotApplicable(
							String.valueOf(searchResult.getDelivaryTimeDuration() / (60)));
				} else {
					response.setDeliveryCharges(String.valueOf(searchResult.getDeliveryCharges()));

					response.setHoursAfterDelieveryChargesNotApplicable(
							String.valueOf(searchResult.getDelivaryTimeDuration() / (60)));
				}

				response.setTotalFuelPrice(calculateFuelPrice(Float.parseFloat(response.getDieselPrice()),
						Float.parseFloat(response.getPetrolPrice()), response.getCar(),
						Integer.parseInt(response.getFuelPriceOffset())));

				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				SimpleDateFormat sdf1 = new SimpleDateFormat("EE,MMM dd,yyyy HH:mm");

				response.setStartDate(sdf1.format(sdf.parse(request.getStartDate())));
				response.setEndDate(sdf1.format(sdf.parse(request.getEndDate())));

				response.setLocationId(request.getLocation());
				List<Location> locations = FECache.getAllLocationsForSearchPage(cityId);
				if (locations != null) {
					for (Location l : locations) {
						if (l.getId().equalsIgnoreCase(request.getLocation())) {
							response.setLocation(l.getName());
						}
					}
					if (response.getLocation() == null) {
						request.setLocation(city.getParentLocation());
						response.setLocationId(city.getParentLocation());
						response.setLocation(FECache.getLocationByLocationId(city.getParentLocation()).getName());
					}
				}

				response.setDelievery(request.getDelivery());
				response.setCarAvailable(true);
			}
		}
		User user = null;
		String source = null;
		if (VolerUtils.getLoggedInUser(httpReq) != null) {
			user = VolerUtils.getLoggedInUser(httpReq);
			if (VolerUtils.checkIfMobileDevice(httpReq) != -1) {
				source = "MSite";
			} else {
				source = "Web";
			}

		}

		HttpServletRequest httpRequest = (HttpServletRequest) httpReq;
		String loginToken = httpRequest.getHeader("loginToken");

		if (loginToken != null && UMSManageLoginToken.getUserByLoginToken(loginToken) != null) {
			user = new User(UMSManageLoginToken.getUserByLoginToken(loginToken).getUser());
			source = VolerUtils.getPlatformOfMobile(httpReq);
		}

		if (user != null && genrateLead) {
			try {

				EmailRequest emailReq = new EmailRequest();

				emailReq.setTemplate(EmailTemplate.BOOKING_ATTEMPTED_SUMMARY);

				emailReq.setCustomerName(user.getFirstname());
				emailReq.setCustomerContactNo(user.getMobile());
				emailReq.setCarModel(response.getCar() != null ? response.getCar().getName() : null);
				emailReq.setStartDate(request.getStartDate());
				emailReq.setEndDate(request.getEndDate());

				if (request.getDelivery() != null && request.getDelivery().trim().equalsIgnoreCase("on")) {

					if (user.getAddress() == null || user.getAddress().isEmpty()) {
						emailReq.setAddress("-");
					} else {
						emailReq.setAddress(user.getAddress());
					}

				} else {
					emailReq.setAddress("Not Opted For Delivery");

				}

				Location l = FECache.getLocationByLocationId(request.getLocation());

				emailReq.setLocation(l.getName());

				emailReq.setMobile(user.getMobile());
				emailReq.setEmail(user.getEmail());
				emailReq.setSource(source);
				City city = FECache.getCityByCityId(cityId);

				String leadEmailString = city.getLeadEmail();
				emailReq.setRecipientEmail(leadEmailString.split(","));

				MandrillMailAPI.sendEmail(emailReq, false);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return response;
	}

	public CarSearchResult checkCarAvailablity(SearchRequest request) throws java.text.ParseException {

		CarSearchResult result = new CarSearchResult();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sdf.parse(request.getStartDate());
			endDate = sdf.parse(request.getEndDate());
		} catch (Exception e) {
			log.error("error while parsing the dates  startdate " + request.getStartDate() + " end date "
					+ request.getEndDate());
			log.error(e.getMessage());
			e.printStackTrace();
			return null;
		}

		Date startDateUtc = DateTimeUtils.istToUtcConverter(startDate);
		Date endDateUtc = DateTimeUtils.istToUtcConverter(endDate);

		if (request.getCarName() != null && request.getCarName().equalsIgnoreCase("all")) {
			request.setCarName(null);
		}

		boolean delivery = (request.getDelivery() != null && request.getDelivery().equalsIgnoreCase("on")) ? true
				: false;

		IPMSSearchResponse ipmsSearchResponse = ipmsService.searchCar(delivery, startDateUtc, endDateUtc,
				request.getCarName(), request.getCity(), request.isNeedCachedData(), request.getVendorId(),
				request.getVendorType(), request.isNeedAvailabilityHoles(), request.getPricingInfo(),
				request.isvDealApplied(), request.getCarId(), request.getPromoRecommendation());
		result.setAvailableCars(ipmsSearchResponse.getAvailableCars());
		result.setUnavailableCars(ipmsSearchResponse.getUnAvailableCars());
		result.setPartialyAvailableCars(ipmsSearchResponse.getPartialAvailableCars());

		result.setStartTime(request.getStartDate());
		result.setEndTime(request.getEndDate());
		result.setDeliver(request.getDelivery());

		result.setDelivaryTimeDuration(ipmsSearchResponse.getDelivaryTimeDuration());
		result.setDeliveryCharges(ipmsSearchResponse.getDeliveryCharges());
		result.setFreeDelivery(ipmsSearchResponse.isFreeDelivery());

		result.setStartTat(ipmsSearchResponse.getStartTat());
		result.setMinimumAllowTimeFromCurrentTimeInMinutes(
				ipmsSearchResponse.getMinimumAllowTimeFromCurrentTimeInMinutes());
		result.setEndTat(ipmsSearchResponse.getEndTat());
		result.setCancellationSlabOne(ipmsSearchResponse.getCancellationSlabOne());
		result.setCancellationSlabTwo(ipmsSearchResponse.getCancellationSlabTwo());
		result.setTotalParitalAvailableCars(ipmsSearchResponse.getPartialAvailableCars().size());

		// result.setSpecialWeekendApplied(ipmsSearchResponse.isSpecialWeekEndApplied());
		TimeDifference diff = DateTimeUtils.getDateDifferenceInString(startDate, endDate);
		result.setTimeDiffer(diff);
		String timeString = " ";
		if (diff.getDays() > 0) {
			timeString += diff.getDays() + " Day(s) ";
		}
		if (diff.getHours() > 0 && diff.getMinutes() > 0) {
			timeString += diff.getHours() + ".5 Hour(s) ";
		} else if (diff.getMinutes() > 0) {
			timeString += "0.5 Hours ";
		} else if (diff.getHours() > 0) {
			timeString += diff.getHours() + " Hour(s) ";
		}
		result.setTimeForSearchPage(timeString);

		return result;
	}

	static class CarComparator implements Comparator<Cars> {
		public int compare(Cars c1, Cars c2) {
			return Integer.compare(c1.getTotalPriceForSearchPage(), (c2.getTotalPriceForSearchPage()));
		}
	}

	public int calculateBabySeatPrice(int babySeatPricePerDay, TimeDifference diff) {

		int totalBabySeatPrice = 0;

		if (diff.getDays() == 0) {
			totalBabySeatPrice = babySeatPricePerDay;
		} else if (diff.getDays() > 0 && (diff.getHours() > 0 || diff.getMinutes() > 0)) {
			totalBabySeatPrice = (babySeatPricePerDay * (diff.getDays() + 1));
		} else if (diff.getDays() > 0 && (diff.getHours() == 0 && diff.getMinutes() == 0)) {
			totalBabySeatPrice = (babySeatPricePerDay * (diff.getDays()));
		}
		return totalBabySeatPrice;

	}

	public int calculateFuelPrice(float dieselPrice, float petrolPrice, Cars car, int fuelOffset) {

		float fuelPrice = 0;

		if (car.getFueltype().equalsIgnoreCase("Diesel")) {
			fuelPrice = car.getFuelTankCapacity() * dieselPrice + fuelOffset;
		} else {
			fuelPrice = car.getFuelTankCapacity() * petrolPrice + fuelOffset;
		}
		return (int) Math.ceil(fuelPrice);
	}

	private Cars checkForZeroSDVariant(Cars car, SummaryRequest request, String cityId) throws Exception {

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		
		int maxMonth = commonComponent.getVisibleCalendarMonths();

		if (car.isZeroSdEndVarient()) {

			Calendar cal = new GregorianCalendar();
			Date endDate = sdf.parse(request.getEndDate());
			cal.setTime(endDate);
			cal.add(Calendar.MINUTE, (int) car.getMinutesToAddForZeroSD());
			Date newEndDate = cal.getTime();

			DateValidationRequest validationRequest = new DateValidationRequest();
			validationRequest.setEndDate(sdf.format(newEndDate));
			validationRequest.setStartDate(request.getStartDate());
			validationRequest.setCityId(cityId);
			validationRequest.setMhb(ipmsService.getMHBForCity(cityId));
			validationRequest.setTat(ipmsService.getTATForCity(cityId));
			validationRequest.setMinMonthsVisibility(maxMonth);

			String message = DateTimeValidation.validateDateTime(validationRequest);

			if (message == null) {
				car.setNewStartDateTimeForZeroSD(request.getStartDate());
				car.setNewEndDateTimeForZeroSD(sdf.format(newEndDate));

				String duration = "";
				int addHours = (int) (car.getMinutesToAddForZeroSD() / 60);
				int addMins = (int) car.getMinutesToAddForZeroSD() % 60;
				if (addHours > 0) {
					duration += addHours + " Hour(s) ";
				}
				if (addMins > 0) {
					duration += " 30 Mins";
				}
				car.setZeroSdDurationString(duration);
			}

		} else if (car.isZeroSdStartVarient()) {

			Calendar cal = new GregorianCalendar();
			Date startDate = sdf.parse(request.getStartDate());
			cal.setTime(startDate);
			cal.add(Calendar.MINUTE, -(int) car.getMinutesToAddForZeroSD());
			Date newStartDate = cal.getTime();

			DateValidationRequest validationRequest = new DateValidationRequest();
			validationRequest.setEndDate(request.getEndDate());
			validationRequest.setStartDate(sdf.format(newStartDate));
			validationRequest.setCityId(cityId);
			validationRequest.setMinMonthsVisibility(maxMonth);

			String message = DateTimeValidation.validateDateTime(validationRequest);

			if (message == null) {
				car.setNewStartDateTimeForZeroSD((sdf.format(newStartDate)));
				car.setNewEndDateTimeForZeroSD(request.getEndDate());
				String duration = "";
				int addHours = (int) (car.getMinutesToAddForZeroSD() / 60);
				int addMins = (int) car.getMinutesToAddForZeroSD() % 60;
				if (addHours > 0) {
					duration += addHours + " Hour(s) ";
				}
				if (addMins > 0) {
					duration += " 30 Mins";
				}
				car.setZeroSdDurationString(duration);
			}
		}
		return car;
	}

	private void updateInstalmentsAsPerCouponApplied(VFlexiInstalmentDetail instalmentAmount, int vmiles,
			int couponAmount) {

		if (instalmentAmount != null) {
			int applicableCouponAmount = 0;
			int instalment = 0;
			if (instalmentAmount.getvFlexiInstalmentBreakup() != null
					&& instalmentAmount.getvFlexiInstalmentBreakup().size() > 0) {
				applicableCouponAmount = couponAmount / (1 + instalmentAmount.getvFlexiInstalmentBreakup().size());
			} else {

				applicableCouponAmount = couponAmount;
			}

			if (instalmentAmount.getvFlexiInstalmentBreakup() != null
					&& instalmentAmount.getvFlexiInstalmentBreakup().size() > 0) {

				for (VolerInstalmentModel model : instalmentAmount.getvFlexiInstalmentBreakup()) {

					float amount = model.getInstallmentPrice();

					amount = amount - applicableCouponAmount;

					instalment = instalment + applicableCouponAmount;
					model.setInstallmentPrice(amount);
				}
			}

			float firstInstalment = instalmentAmount.getFirstVflexiInstalment();
			instalmentAmount.setFirstVflexiInstalment(firstInstalment + applicableCouponAmount - instalment - vmiles);

		}

	}

	private void updateVmiles(VFlexiInstalmentDetail detail, int vmiles) {

		float firstInstalment = detail.getFirstVflexiInstalment() - vmiles;
		detail.setFirstVflexiInstalment(firstInstalment);

	}
}

