package tree;

import java.util.ArrayList;
import java.util.List;

public class CommonAncestorProblem {

	Node root;

	public static void main(String args[]) {

		CommonAncestorProblem common = new CommonAncestorProblem();

		common.root = new Node(1);
		common.root.left = new Node(2);
		common.root.right = new Node(3);
		common.root.left.left = new Node(4);
		common.root.left.right = new Node(5);
		common.root.right.left = new Node(6);
		common.root.right.right = new Node(7);

		List<Integer> list1 = new ArrayList<Integer>();

		List<Integer> list2 = new ArrayList<Integer>();

		boolean first = traverseTRee(common.root, 4, list1);

		boolean second = traverseTRee(common.root, 5, list2);

		int i = -1;
		if (first && second) {

			for (i = 0; i < list1.size() && i < list2.size(); i++) {

				if ( !(list1.get(i) == list2.get(i))) {

					break;
				}
			}

			System.out.println(list1.get(i - 1));
		}
		else{
		System.out.println(i);
		}
	}

	public static boolean traverseTRee(Node head, int value, List<Integer> list) {

		if (head == null) {

			return false;
		}
		list.add(head.data);

		if (head.data == value) {

			return true;
		}
		if (head != null && traverseTRee(head.left, value, list)) {

			return true;
		}

		if (head != null && traverseTRee(head.right, value, list)) {

			return true;
		}

		list.remove(list.size() - 1);
		return false;
	}

}
