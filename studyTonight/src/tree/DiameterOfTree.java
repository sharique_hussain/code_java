package tree;

public class DiameterOfTree {

	Node root;

	public static void main(String args[]) {

		DiameterOfTree tree = new DiameterOfTree();

		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

	

		System.out.println("Dia : " + getDiameterOfTree(tree.root));

	}

	
	public static int getDiameterOfTree(Node root){
		
		if(root==null){
			
			return 0;
		}
		
		int lheight=getHeight(root.left);
		int rheight=getHeight(root.right);
		
		int ldia=getDiameterOfTree(root.left);
		
		int rdia=getDiameterOfTree(root.right);
		
		return Math.max(lheight+rheight+1,Math.max(ldia,rdia));
		
	}
	
public static int getHeight(Node root){
	
	if(root==null){
		 
		return 0;
		
	}
	
	int lheight=getHeight(root.left);
	
	int rheight=getHeight(root.right);
	
	
	if(lheight>rheight){
		return lheight+1;
	}
	else{
		
		return rheight+1;
	}
	
}
	
}
