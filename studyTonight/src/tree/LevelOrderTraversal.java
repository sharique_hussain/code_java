package tree;

import java.util.HashSet;
import java.util.Set;

class Nodes {
	int data;
	Nodes left, right;

	public Nodes(int item) {
		data = item;
		left = right = null;
	}
}

public class LevelOrderTraversal {

	Nodes root;
	
	private static int check;

	public static void main(String args[]) {
	//	LevelOrderTraversal tree = new LevelOrderTraversal();
		
		Set<String> set=new HashSet<String>();
		
	int	a=1;
	int b=2;
	int c=3;
	a|=4;
	b >>=1;
	c <<=1;
	a ^=c;
	System.out.println(a+" "+b+" "+c);
	
	System.out.println(true|false);
	
	System.out.println(fun());
		
	/*	tree.root = new Nodes(1);
		tree.root.left = new Nodes(2);
		tree.root.right = new Nodes(3);
		tree.root.left.left = new Nodes(4);
		tree.root.left.right = new Nodes(5);

		System.out.println("Level order traversal of binary tree is ");
		traverseLevelOrder(tree.root);
		System.out.println("Left Subtree view");

		printLeftSubtree(tree.root);
		System.out.println("Right Subtree view");

		rightSubTree(tree.root);
		System.out.println("Inorder Traversal");
        inOrderTraversal(tree.root);
		System.out.println("PreOrder Traversal");
		preOrderTraversal(tree.root);

		System.out.println("POst Order Traversal");
		postOrderTraversal(tree.root);*/
	}

	public static void printLevelOrderTraversal(Nodes head, int level) {

		if (head == null) {

			return;
		}

		if (level == 1) {

			System.out.println(head.data);

		}

		else if (level > 1) {

			printLevelOrderTraversal(head.left, level - 1);

			printLevelOrderTraversal(head.right, level - 1);

		}

	}

	public static int getHeightOfTree(Nodes head) {

		if (head == null) {
			return 0;
		}
		int lheight = getHeightOfTree(head.left);
		int rheight = getHeightOfTree(head.right);

		if (lheight > rheight) {
			return lheight + 1;
		} else
			return rheight + 1;

	}

	public static void traverseLevelOrder(Nodes head) {

		int height = getHeightOfTree(head);

		for (int i = 1; i <= height; i++) {

			printLevelOrderTraversal(head, i);
		}
	}

	public static void printLeftSubtree(Nodes head) {

		if (head == null) {
			return;
		}
		System.out.println(head.data);

		printLeftSubtree(head.left);

	}

	public static void rightSubTree(Nodes head) {

		if (head == null) {
			return;
		}
		System.out.println(head.data);

		rightSubTree(head.right);
	}

	public static void inOrderTraversal(Nodes head) {

		if (head == null) {

			return;
		}

		inOrderTraversal(head.left);
		System.out.println(head.data);
		inOrderTraversal(head.right);

	}

	public static void preOrderTraversal(Nodes head) {

		if (head == null) {

			return;
		}
		System.out.println(head.data);
		inOrderTraversal(head.left);
		inOrderTraversal(head.right);

	}

	public static void postOrderTraversal(Nodes head) {

		if (head == null) {

			return;
		}

		inOrderTraversal(head.left);
		inOrderTraversal(head.right);
		System.out.println(head.data);

	}
	
	public static int fun(){
	return	++check;
	}

}
