package tree;

import java.util.ArrayList;

public class Recursion {

	public static void main(String[] args) {

		ArrayList<Integer> list1 = new ArrayList<Integer>();
		ArrayList<Integer> list2 = new ArrayList<Integer>();

		list1.add(6);
		list1.add(4);
	//	list1.add(3);
	//	list1.add(7);
		list2.add(7);
		list2.add(5);
//		list2.add(8);
//		list2.add(4);

		ArrayList<Integer> temp = new ArrayList<Integer>();
		findUniquePair(list1, 0, list2, temp);
	}

	public static void findUniquePair(ArrayList<Integer> list1, int index, ArrayList<Integer> list2,
			ArrayList<Integer> temp) {

		try {
			Integer itemFromList1 = list1.get(index);

			Integer matchedItemFromList2 = getMatchedItemFromList2(itemFromList1, list2, temp);
			if (matchedItemFromList2 != null) {
				System.out.println("Pair Matched " + itemFromList1 + " & " + matchedItemFromList2);
				temp.add(matchedItemFromList2);
			}

			// Recursive Call
			findUniquePair(list1, ++index, list2, temp);

		} catch (IndexOutOfBoundsException e) {
			System.out.println("No more items are in the list");
			System.exit(0);
		}

	}

	static Integer getMatchedItemFromList2(int itemFromList1, ArrayList<Integer> list2, ArrayList<Integer> temp) {

		for (Integer i : list2) {
			if (!temp.contains(i) && itemFromList1 + i >= 10) {
				return i;
			}
		}

		return null;
	}

}
