package tree;

public class SortedArrayToBST {

	public static void main(String args[]) {

		
		
		
		int[] array={1,2,3,4,5,6,7};
		
		Node root= arrayToBst(array,0,array.length-1);
		
		preorder(root);
	}

	public static Node arrayToBst(int[] array, int start, int end) {

		if (start > end) {

			return null;
		}

		int middle = (start + end) / 2;

		Node node = new Node(array[middle]);

		node.left = arrayToBst(array, start, middle-1);
		node.right = arrayToBst(array, middle + 1, end);

		return node;
	}

	public static void preorder(Node root) {

		if (root == null) {
			return;
		}

		System.out.println(root.data);

		preorder(root.left);

		preorder(root.right);

	}
}
