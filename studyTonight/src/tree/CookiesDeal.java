package tree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CookiesDeal {

	public static void main(String args[]) throws FileNotFoundException {

		// Taking Input From User

		Map<Integer, Integer> inputCookieDeals = new HashMap<Integer, Integer>();
		Scanner scanner = new Scanner(System.in);
		
		File file=new File("case2.txt");
		scanner=new Scanner(file);

		int numberOfDeals = scanner.nextInt();
		int maxCookies = scanner.nextInt();
		scanner.nextLine();

		// Assuming that n & m>0

		for (int i = 0; i < numberOfDeals; i++) {

			int dealCookie = scanner.nextInt();
			int dealPrice = scanner.nextInt();

			// Putting Only Those DEals which have equal or less cookie count
			// than maximum cookie for evluation

			if (dealCookie == 0 && dealPrice == 0) {

				scanner.nextLine();
				break;

			}
			if (dealCookie <= maxCookies) {

				inputCookieDeals.put(dealCookie, dealPrice);
			}
			scanner.nextLine();
		}

		System.out.println(getTheBestDeal(inputCookieDeals));

	}

	private static String getTheBestDeal(Map<Integer, Integer> cookieMap) {

		if (cookieMap != null && cookieMap.size() > 0) {

			int cookie = 0;
			int price = 0;
			float ratio = 0;
			for (int cookieCount : cookieMap.keySet()) {

				int dealPrice = cookieMap.get(cookieCount);
				float ratioChecker = (cookieCount * 1.f) / dealPrice;

				// Checking for the best deal and if two deals are equal deal
				// with lesser number of cookies is selected
				if (ratioChecker == ratio) {

					if (cookieCount < cookie) {
						cookie = cookieCount;
						price = dealPrice;
						ratio = ratioChecker;
					}

				}
				// Starting with the first deal
				else if (ratio == 0) {
					cookie = cookieCount;
					price = dealPrice;
					ratio = ratioChecker;

				}
				// Checking for the best deal on the basis of ratio
				else if (ratioChecker < ratio) {

					cookie = cookieCount;
					price = dealPrice;
					ratio = ratioChecker;
				}
			}

			return "Purchase " + cookie + " cookies for " + "$" + price;

		} else {

			return "  No good deals";
		}
	}

}
