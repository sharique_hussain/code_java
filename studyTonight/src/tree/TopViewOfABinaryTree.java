package tree;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class TopViewOfABinaryTree {

	public static void main(String args[]) {

		Node root = new Node(1);
		root.left = new Node(2);
		root.right = new Node(3);
		root.left.right = new Node(4);
		root.left.right.right = new Node(5);
		root.left.right.right.right = new Node(6);
		
		TopViewOfABinaryTree t=new TopViewOfABinaryTree();
		
		t.topView(root);

	}

public  void  topView(Node root) {

		if (root == null) {

			return;
		}
		Set<Integer> set = new HashSet<Integer>();
		Queue<QNode> queue = new LinkedList<QNode>();
		queue.add(new QNode(0, root));
		while (!queue.isEmpty()) {

			QNode qnode = queue.remove();
			int dist = qnode.dist;
			Node node = qnode.node;

			if (!set.contains(dist)) {

				set.add(dist);
				System.out.print(node.data + " ");
			}

			if (node.left != null) {

				queue.add(new QNode(dist - 1, node.left));
			}
			if (node.right != null) {
				queue.add(new QNode(dist + 1, node.right));
			}

		}

	}

	class QNode {

		int dist;
		Node node;

		public QNode(int hd, Node node) {
			this.dist = hd;
			this.node = node;
		}
	}
}
