package tree;

import java.util.LinkedList;
import java.util.Queue;

public class Revision {

	Node root;

	public static void main(String args[]) {

		Revision tree = new Revision();

		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

		int height = getHeightOfAtree(tree.root);
		// System.out.println(height);

		// System.out.println(getDiameterOfTree(tree.root));

		for (int i = 1; i <= height; i++) {

			printLevelOrderTraversal(tree.root, i);
		}

		System.out.println();
		System.out.println("----------------------------------");
		
		levelOrderWithoutRecursion(tree.root);
	}

	public static int getHeightOfAtree(Node node) {

		if (node == null) {
			return 0;
		}

		int lheight = getHeightOfAtree(node.left);
		int rheight = getHeightOfAtree(node.right);

		if (lheight > rheight) {
			return lheight + 1;
		}

		return rheight + 1;

	}

	public static int getDiameterOfTree(Node node) {

		if (node == null) {

			return 0;
		}
		int lheight = getHeightOfAtree(node.left);
		int rheight = getHeightOfAtree(node.right);

		int ldia = getDiameterOfTree(node.left);

		int rdia = getDiameterOfTree(node.right);

		return Math.max(lheight + rheight + 1, Math.max(ldia, rdia));

	}

	public static void printLevelOrderTraversal(Node node, int level) {

		if (node == null) {

			return;
		}
		if (level == 1) {

			System.out.print(node.data);
		} else if (level > 1) {
			printLevelOrderTraversal(node.left, level - 1);
			printLevelOrderTraversal(node.right, level - 1);
		}

	}

	public static void levelOrderWithoutRecursion(Node node) {

		Queue<Node> queue = new LinkedList<Node>();

		queue.add(node);

		while (!queue.isEmpty()) {

			Node newNode = queue.poll();
			System.out.print(newNode.data);

			if (newNode.left != null) {
				queue.add(newNode.left);
			}
			if (newNode.right != null) {
				queue.add(newNode.right);
			}

		}

	}

}
