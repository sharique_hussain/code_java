package tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class TwoListUsingRecursion {

	public static void main(String args[]) {

		List<Integer> list1 = new ArrayList<Integer>();
		list1.add(6);
		list1.add(4);
		list1.add(7);
		List<Integer> list2 = new ArrayList<Integer>();
		list2.add(7);
		list2.add(5);
		list2.add(9);
		int count = list1.size();
		List<Integer> clonelist1;
		List<Integer> clonelist2;
		Map<Integer, Integer> newMap = new HashMap<Integer, Integer>();
		boolean bool = false;

		for (int i = 0; i < list1.size() * list2.size(); i++) {

			list1 = getSwappedList(list1);
			list2 = getSwappedList(list2);
			clonelist1 = new ArrayList<Integer>(list1);
			clonelist2 = new ArrayList<Integer>(list2);

			Map<Integer, Integer> result = getSum(clonelist1, clonelist2, 0, 0, list1.size() * list2.size(), newMap);

			if (count == result.size()) {

				System.out.println("Allocated");
				print(newMap);
				bool = true;
				break;
			}
			result.clear();
		}
		if (!bool) {
			System.out.println("Not Allocated");
		}

	}

	public static Map<Integer, Integer> getSum(List<Integer> list1, List<Integer> list2, int i, int count,
			int totalPermutations, Map<Integer, Integer> map) {

		if (list1.size() == 0 || list2.size() == 0 || totalPermutations < count) {

			return map;

		}
		if (i < list1.size() && i < list2.size() && list1.get(0) + list2.get(i) > 10) {

			map.put(list1.get(0), list2.get(i));
			list1.remove(0);
			list2.remove(i);
			i = -1;
		}

		return getSum(list1, list2, i + 1, count + 1, totalPermutations, map);

	}

	public static void print(Map<Integer, Integer> map) {

		if (map.size() > 0) {
			for (int count : map.keySet()) {

				System.out.println(count + "-" + map.get(count));
			}
		}
	}

	public static List<Integer> getSwappedList(List<Integer> list) {

		Random r = new Random();
		int low = 0;
		int high = list.size() - 1;
		int result = r.nextInt(high - low) + low;
		if (result < list.size() - 1) {
			Collections.swap(list, result, result + 1);
		} else {
			if (result > 0) {
				Collections.swap(list, result - 1, result);
			}

		}
		return list;
	}
}
