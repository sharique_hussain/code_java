package binarySearchTree;

public class InsertingInBst {

	Node root;

	public static void main(String args[]) {

		InsertingInBst tree = new InsertingInBst();
		tree.root = new Node(50);
		// tree.insertInAbst(tree.root, 50);
		tree.insertInAbst(tree.root, 30);
		tree.insertInAbst(tree.root, 20);
		tree.insertInAbst(tree.root, 40);
		tree.insertInAbst(tree.root, 70);
		tree.insertInAbst(tree.root, 60);
		tree.insertInAbst(tree.root, 80);

		tree.inorderTraversal(tree.root);
	}

	public Node insertNode(int key) {

		Node node = new Node(key);

		return node;
	}

	public void inorderTraversal(Node node) {

		if (node == null) {
			return;
		}
		inorderTraversal(node.getLeft());
		System.out.println(node.getData());
		inorderTraversal(node.getRight());
	}

	public Node insertInAbst(Node root, int key) {

		if (root == null) {

			root = insertNode(key);
			return root;
		}

		if (root.getData() > key) {

			root.setLeft(insertInAbst(root.getLeft(), key));

		} else {

			root.setRight(insertInAbst(root.getRight(), key));

		}

		return root;
	}

	public void deleteAGivenNode(Node node, int key) {

		if (node == null) {
			return;
		}
		if(node.getData()==key){
			
			
			
		}

		if (node.getData() > key) {

			
		} else if (node.getData() < key) {

		}
	}
}
