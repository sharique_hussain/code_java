package HashMap;

import java.util.HashMap;
import java.util.Map;

public class Test {

	

	public static void main(String args[]) {

		Map<Checker,Integer> map=new HashMap<Checker,Integer>();
		
		Checker checker1=new Checker("aditi",29);
		System.out.println("Name : "+checker1.getName()+" "+checker1.hashCode());
		map.put(checker1, 1);
		Checker checker2=new Checker("shruti",32);
		map.put(checker2, 2);
		System.out.println("Name : "+checker2.getName()+" "+checker2.hashCode());
		Checker checker3=new Checker();
		map.put(checker3, 3);
		System.out.println("Name : "+checker3.getName()+" "+checker3.hashCode());
		Checker checker4=checker2;
		System.out.println("Name : "+checker4.getName()+" "+checker4.hashCode());
		map.put(checker4, 4);
		System.out.println(map);
		
		
		
		
	}
}
