package DynamicProgramming;

public class RodCuttingProblem {

	public static int counter = 1;

	public static void main(String args[]) {

		int arr[] = new int[] { 1, 5, 8, 9, 10, 17, 17, 20};
		int size = arr.length;
//		System.out.println("Maximum Obtainable Value is " + calculateMaxAmount(arr, size));
		
		System.out.println("Maximum Obtainable Value is " + rodCuttingUsingDp(arr, size));

	}

	public static int calculateMaxAmount(int[] price, int size) {

		if (size <= 0) {
			return 0;
		}
		int maxAmount = Integer.MIN_VALUE;

		for (int i = 0; i < size; i++) {

			maxAmount = Math.max(maxAmount, price[i] + calculateMaxAmount(price, size - i - 1));

			System.out.println("Max Amount Value : " + maxAmount + " Counter Value : " + counter++);

		}

		return maxAmount;
	}

	public static int rodCuttingUsingDp(int[] price, int n) {

		int[] dp = new int[n + 1];

		dp[0] = 0;

		for (int i = 1; i <= n; i++) {
			int max = -10;
			for (int j = 0; j < i; j++) {

				max = Math.max(max, price[j] + dp[i - j - 1]);
				
				  dp[i] = max;
			}

		}

		return dp[n];

	}
}
