package DynamicProgramming;

public class SumFinder {

	public static void main(String args[]){
		
		int set[] = {3, 34, 4, 12, 5, 2};
        int sum = 9;
        int n = set.length;
        if (sumChecker(set, n, sum) == true)
           System.out.println("Found a subset with given sum");
        else
           System.out.println("No subset with given sum");
		
		
	}
	
	public static boolean sumChecker(int[] array,int n,int sum){
		
		if(sum==0){
			
			return true;
		}
		if(n==0 && sum !=0){
			
			return false;
		}
		
		//Check if last element is greater than sum
		if(array[n-1]>sum){
			
			return sumChecker(array,n-1,sum);
		}
		 /* else, check if sum can be obtained by any of the following
        (a) including the last element
        (b) excluding the last element   */
		
		return sumChecker(array,n-1,sum)||sumChecker(array,n-1,sum-array[n-1]);
		
	}
	
	
}
