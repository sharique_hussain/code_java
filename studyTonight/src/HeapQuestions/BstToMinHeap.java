package HeapQuestions;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import tree.DiameterOfTree;
import tree.Node;

public class BstToMinHeap {

	Node root;

	public static void main(String args[]) {

		BstToMinHeap tree = new BstToMinHeap();

		Object obj;
		String str;
		StringBuilder sb;
		StringBuffer sbr;
		tree.root = new Node(4);
		tree.root.left = new Node(2);
		tree.root.right = new Node(6);
		tree.root.left.left = new Node(1);
		tree.root.left.right = new Node(3);
		tree.root.right.left = new Node(5);
		tree.root.right.right = new Node(7);
		System.out.println("Inorder Traversal of Bst");
		printInOrderTraversal(tree.root);
		List<Integer> array = new ArrayList<Integer>();
		getInOrderTraversl(tree.root, array);
		convertToMinHeap(tree.root, array, -1);
		System.out.println("Inorder Traversal of MIn Heap");
		printInOrderTraversal(tree.root);
	}

	public static void getInOrderTraversl(Node head, List<Integer> array) {

		if (head == null) {

			return;
		}
		getInOrderTraversl(head.left,array);
		array.add(head.data);
		getInOrderTraversl(head.right,array);
	}

	public static void printInOrderTraversal(Node head) {

		if (head == null) {

			return;
		}
		printInOrderTraversal(head.left);
		System.out.println(head.data);
		printInOrderTraversal(head.right);

	}

	public static void convertToMinHeap(Node head, List<Integer> list, int i) {

		if (head == null || i>=list.size()) {
			return;
		}

		head.data = list.get(++i);

		convertToMinHeap(head.left, list, i);

		convertToMinHeap(head.right, list, i);

	}
}
