package SimpleRecursions;

public class SumOfArrays {

	public static void main(String args[]) {

		int[] array = { 3, 4, 5, 6, 7, 2 };

		System.out.println(giveSum(array, 0));
	}

	public static int giveSum(int[] array, int i) {

		if (array.length == 0 || i >= array.length) {
			return 0;
		}

		return array[i] + giveSum(array, i + 1);

	}

}
