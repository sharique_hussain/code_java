package SimpleRecursions;

public class FibonacciSeries {

	public static void main(String args[]) {

		System.out.println(printfibonacciSeries(6));
		System.out.println(printSeries(6));
		System.out.println(fib(6));
		
	}

	// time complexity is exponential T(N)=T(N-1)+T(N-2)
	// =O(2^n-1)+O(2^n-2)=O(2^n)
	public static int printfibonacciSeries(int n) {

		if (n <= 1) {

			return n;
		}

		return printfibonacciSeries(n - 1) + printfibonacciSeries(n - 2);

	}

	// using dynamic programming time complexity=0(N) Space Complexity=O(N)
	public static int printSeries(int n) {

		int[] array = new int[n + 1];
		array[0] = 0;
		array[1] = 1;
		for (int i = 2; i < n + 1; i++) {

			array[i] = array[i - 1] + array[i - 2];

		}

		return array[n];

	}

	// fibonacci without array
	public static int fib(int n) {

		int a = 0, b = 1, c;

		for (int i = 2; i <= n; i++) {

			c = a + b;
			a = b;
			b = c;
		}

		return b;

	}
	
	

}
