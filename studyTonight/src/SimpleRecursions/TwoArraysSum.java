package SimpleRecursions;

public class TwoArraysSum {

	public static void main(String args[]) {

		int[] array1 = { 1, 2, 3, 4, 5 };
		int[] array2 = { 5, 4, 3, 2, 1 };

		printSumAtIndex(array1, array2, 0);

	}

	public static void printSumAtIndex(int[] a, int[] b, int i) {

		if (i >= a.length) {
			return;
		}

		System.out.println(a[i] + b[i]);
		printSumAtIndex(a, b, i + 1);
	}
}
