package SimpleRecursions;

public class LeafNodeCount {

	Node root;

	public static void main(String args[]) {

		LeafNodeCount tree = new LeafNodeCount();

		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

		System.out.println("Count Of Leaf Node is : " + tree.getLeafNode(tree.root));
	}

	public int getLeafNode(Node root) {

		if (root == null) {
			return 0;
		}
		if (root.left == null && root.right == null) {
			return 1;
		}

		return getLeafNode(root.left) + getLeafNode(root.right);

	}
}
