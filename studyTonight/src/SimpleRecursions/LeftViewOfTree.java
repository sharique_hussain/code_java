package SimpleRecursions;

public class LeftViewOfTree {

	Node root;

	public static void main(String args[]) {

		LeftViewOfTree tree = new LeftViewOfTree();

		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

		if (tree.root != null) {
			System.out.println(tree.root.data);
		}
		printLeftVief(tree.root);
	}

	public static void printLeftVief(Node root) {

		if (root == null) {
			return;
		}
		if (root.left != null) {
			System.out.println(root.left.data);
		}
		printLeftVief(root.left);
	}
}
