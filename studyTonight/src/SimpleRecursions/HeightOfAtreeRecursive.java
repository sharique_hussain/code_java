package SimpleRecursions;

 class Node {
	int data;
	Node left, right;

	Node(int item) {
		data = item;
		left = right = null;
	}
}

public class HeightOfAtreeRecursive {

	Node root;

	public static void main(String ags[]) {

		HeightOfAtreeRecursive tree = new HeightOfAtreeRecursive();

		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

		System.out.println("Height of tree is : " + tree.getHeightOfATree(tree.root));
	}

	public int getHeightOfATree(Node root) {

		if (root == null) {
			return 0;
		}
		int ldepth = getHeightOfATree(root.left);
		int rdepth = getHeightOfATree(root.right);

		if (ldepth > rdepth) {
			return ldepth + 1;
		} else
			return rdepth + 1;

	}
}
