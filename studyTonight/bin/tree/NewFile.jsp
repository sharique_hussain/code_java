<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.voler.utils.VolerAuthConstant"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="A Components Mix Bootstarp 3 Admin Dashboard Template">

<meta name="author" content="Westilian">
<title>Add Inventory</title>

<tiles:insertAttribute name="head" />

<script language="javascript" type="text/javascript">
    window.history.forward();
</script>
</head>
<body onload="disableBackButton();">
	<div class="page-container hide-list-menu">
		<!--Leftbar Start Here -->

		<tiles:insertAttribute name="header" />

		<!--Topbar Start Here -->


		<c:set var="countOfSDDiv" value="0"></c:set>
		<c:set var="counterOfVDealsDiv" value="0"></c:set>
		<c:set var="counterOfCarLeftDiv" value="0"></c:set>


		<c:set var="countOfPartialPaymentDiv" value="0"></c:set>




		<div class="page-content">
			<!--Topbar Start Here -->
			<tiles:insertAttribute name="left-nav" />
			<!-- <header class="top-bar"> -->
			<%-- 	<div class="container-fluid top-nav">
					
									</div>
								</div>
							</div>
						</div>

						<tiles:insertAttribute name="myAccount" />


					</div>
				</div> --%>

			</header>






			<!--main content Start Here -->
			<div class="main-container">
				<form class="form-horizontal" action="addInventory" method="post"
					onsubmit="return validateIfNeedToSubmit()">
					<c:forEach items="${dtoList}" var="dtoList" varStatus="status">
						<div class="container-fluid">
							<div class="main-tab-content tab-content">
								<div id="divIDNo${status.index}">

									<div class="container-fluid">
										<div class="row">
											<div class="col-md-12">
												<div class="add-inventory-section">
													<div class="row">
														<div class="col-md-6">

															<font size="4" color="gray">Car Model :
																${dtoList.name} | Length : ${dtoList.length} | Engine
																Capacity : ${dtoList.engineCapacity}</font> <br> <input
																type="text"
																name="carModelWithPricing[${status.index}].volerCarModelId"
																value="${dtoList.volerCarModelId}" style="display: none">
															<input type="text" id="vendorId"
																name="carModelWithPricing[${status.index}].vendorId"
																value="${dtoList.vendorId}" style="display: none" /> <input
																type="text"
																name="carModelWithPricing[${status.index}].vendorCarModelId"
																value="${dtoList.vendorCarModelId}"
																style="display: none"> <input type="hidden"
																name="carModelWithPricing[${status.index}].defaultVendorId"
																value="${dtoList.defaultVendorId}"> <input
																type="hidden" name="" id="numberOfModels"
																value="${numberOfModels}"> <input id="check"
																name="carModelWithPricing[${status.index}].check"
																value="true" style="display: none" /> <input
																id="pageChecker"
																name="carModelWithPricing[${status.index}].pageChecker"
																value="${pageChecker}" value="true"
																style="display: none" /> <input type="hidden"
																id="minimumMinsOfBooking"
																value="${minimumMinsOfBooking}">
															<textarea rows="10" cols="10" style="display: none;"
																id="coloursForCarLeftArr">${coloursForCarLeftArr} </textarea>

														</div>

														<label class=" col-md-3 text-right pricing-label"
															style="color: gray">Select Pricing Type</label>
														<div class="col-md-3">
															<select id="select${status.index}"
																name="carModelWithPricing[${status.index}].pricingType"
																onchange="GetSelectedTextValue(this)"
																class="form-control  select-price-type  select2">


																<option value="${dtoList.pricingType}"
																	id="selectedValue${status.index}"
																	style="pointer-events: none; cursor: default;" selected
																	value="${dtoList.pricingType}">${dtoList.pricingType}</option>
																<c:forEach items="${pricingType}" var="pricing">
																	<%-- --%>
																	<c:if test="${dtoList.pricingType != pricing}">
																		<option value="${pricing}">${pricing}</option>
																	</c:if>

																</c:forEach>

															</select>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>




									<div id="regularTariff${status.index}"
										class="full-tab-container regularTariff" style="display: none">

										<div class="main-tab-content tab-content">


											<div id="regularTariff" class="tab-pane active">
												<%--   <form class="form-horizontal"> --%>

												<div class="row">
													<div class="col-md-12">
														<div class="form-group" id="regularTariff">
															<div class="col-md-4">

																<c:if
																	test="${dtoList.vatInclusiveFixedPricing ne false}">
																	<input
																		name="carModelWithPricing[${status.index}].vatInclusiveFixedPricing"
																		type="checkbox" class="i-min-check" checked="checked"
																		id="vatInclusiveFixedPricing${status.index}"
																		checked="checked">
																	<font size="3" color=grey> VAT (Select If
																		Inclusive)</font>

																</c:if>

																<c:if test="${dtoList.vatInclusiveFixedPricing ne true}">
																	<input
																		name="carModelWithPricing[${status.index}].vatInclusiveFixedPricing"
																		type="checkbox" class="i-min-check"
																		id="vatInclusiveFixedPricing${status.index}">
																	<font size="3" color=grey> VAT (Select If
																		Inclusive)</font>

																</c:if>



															</div>

															<div class="col-md-4"></div>
															<div class="col-md-4"></div>
														</div>


													</div>
												</div>
												<div class="row">

													<div class="col-md-12">
														<div class="form-group" id="regularTariff"
															class="regularTariff">

															<div class="col-md-4">
																<font size="3" color=grey><label>Hourly
																		Tariff (INR)</label></font> <input type="text"
																	onkeypress="return isNumberKey(event)"
																	id="hourlyTariff${status.index}"
																	value="${dtoList.hourlyTariff}"
																	name="carModelWithPricing[${status.index}].hourlyTariff"
																	class="form-control regTariff${status.index}"
																	placeholder="Enter Price" min="0"></input> <label
																	class="error" style="display: none;"><small>This
																		field is required.</small></label>
															</div>
															<div class="col-md-4">
																<font size="3" color=grey><label>Daily
																		Tariff (INR)</label></font> <input type="text"
																	onkeypress="return isNumberKey(event)"
																	value="${dtoList.dailyTariff}"
																	name="carModelWithPricing[${status.index}].dailyTariff"
																	id="dailyTariff${status.index}"
																	class="form-control regTariff${status.index}"
																	placeholder="Enter Price" min="0"> <label
																	class="error" style="display: none;"><small>This
																		field is required.</small></label>
															</div>
															<div class="col-md-4">
																<font size="3" color=grey><label>Monthly
																		Tariff (INR)</label></font> <input type="text"
																	onkeypress="return isNumberKey(event)"
																	id="monthlyTariff${status.index}"
																	name="carModelWithPricing[${status.index}].monthlyTariff"
																	value="${dtoList.monthlyTariff}"
																	class="form-control regTariff${status.index}"
																	placeholder="Enter Price" min="0"> <label
																	class="error" style="display: none;"><small>This
																		field is required.</small></label>

															</div>

														</div>
													</div>


												</div>


											</div>
										</div>
									</div>

									<%-- discounted tariff starts --%>
									<div id="discountedTariff${status.index}"
										class="full-tab-container discountedTariff"
										style="display: none">

										<div class="main-tab-content tab-content">


											<div id="discountedTariff" class="tab-pane active">
												<%--   <form class="form-horizontal"> --%>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group" id="regularTariff">
															<div class="col-md-4">
																<c:if
																	test="${dtoList.vatInclusiveDiscountedPricing ne false}">
																	<input
																		name="carModelWithPricing[${status.index}].vatInclusiveDiscountedPricing"
																		type="checkbox" class="i-min-check" checked="checked"
																		id="vatInclusiveDiscountedPricing${status.index}">
																	<font size="" 3 color=grey> VAT (Select If
																		Inclusive)</font>
																</c:if>
																<c:if
																	test="${dtoList.vatInclusiveDiscountedPricing ne true}">
																	<input
																		name="carModelWithPricing[${status.index}].vatInclusiveDiscountedPricing"
																		type="checkbox" class="i-min-check"
																		id="vatInclusiveDiscountedPricing${status.index}">
																	<font size="" 3 color=grey> VAT (Select If
																		Inclusive)</font>
																</c:if>

															</div>
															<div class="col-md-4"></div>
															<div class="col-md-4"></div>
														</div>


													</div>
												</div>

												<%-- flat discount starts --%>
												<div class="row">

													<div class="col-md-12">
														<div class="form-group" id="flatDiscountTariff"
															class="dicountedTariff">

															<div class="col-md-3">
																<font size="3" color=grey><label>
																		Discount Percentage (%)</label></font> <input type="number"
																	value="${dtoList.discountInPercentage}"
																	name="carModelWithPricing[${status.index}].discountInPercentage"
																	id="discountInPercentage${status.index}"
																	class="form-control regTariff${status.index}"
																	placeholder="Enter Discount" min="0" max="100">
															</div>
															<div class="col-md-3">
																<label> </label>
															</div>
															<div class="col-md-3"></div>
															<div class="col-md-3">
																<label> </label>
															</div>

														</div>
													</div>

												</div>


												<%-- flat discount ends --%>





											</div>
										</div>
									</div>



									<%-- discounted tariff ends --%>







									<div id="specialTariff${status.index}"
										class="full-tab-container specialTariff" style="display: none">

										<div class="main-tab-content tab-content">

											<div id="specialTariff" class="tab-pane active"
												class="specialTariff">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group" id="regularTariff">
															<div class="col-md-4">
																<c:if
																	test="${dtoList.vatInclusivePerHourPricing ne false}">

																	<input
																		name="carModelWithPricing[${status.index}].vatInclusivePerHourPricing"
																		type="checkbox" class="i-min-check " checked="checked"
																		id="vatInclusivePerHourPricing${status.index}">
																	<font size="3" color=grey> VAT (Select If
																		Inclusive)</font>
																</c:if>


																<c:if
																	test="${dtoList.vatInclusivePerHourPricing ne true}">
																	<input
																		name="carModelWithPricing[${status.index}].vatInclusivePerHourPricing"
																		type="checkbox" class="i-min-check "
																		id="vatInclusivePerHourPricing${status.index}">
																	<font size="3" color=grey> VAT (Select If
																		Inclusive)</font>
																</c:if>


															</div>
															<div class="col-md-4"></div>
															<div class="col-md-4"></div>
														</div>


													</div>
												</div>

												<div class="row">
													<div class="col-md-12">
														<div class="form-group" id="specialTariff"
															class="specialTariff">

															<div class="col-md-4">
																<font size="3" color="grey"> <label>Weekday(INR)</label></font>
																<input type="text"
																	onkeypress="return isNumberKey(event)"
																	id="pricePerHour${status.index}"
																	name="carModelWithPricing[${status.index}].weekDayHourPrice"
																	class="form-control specTariff"
																	placeholder="Enter Price"
																	value="${dtoList.weekDayHourPrice}"> <label
																	class="error" style="display: none;"><small>This
																		field is required.</small></label>
															</div>
															<div class="col-md-4">
																<font size="3" color="grey"><label>Weekend
																		(INR)</label></font> <input type="text"
																	onkeypress="return isNumberKey(event)"
																	id="weekendTariff${status.index}"
																	name="carModelWithPricing[${status.index}].weekEndHourPrice"
																	class="form-control specTariff${status.index}"
																	placeholder="Enter Price"
																	value="${dtoList.weekEndHourPrice}"> <label
																	class="error" style="display: none;"><small>This
																		field is required.</small></label>
															</div>
															<div class="col-md-4">
																<font size="3" color="grey"><label>Peak
																		(INR)</label></font> <input type="text"
																	onkeypress="return isNumberKey(event)"
																	id="peakSeasonTariff${status.index}"
																	name="carModelWithPricing[${status.index}].peakHourPrice"
																	class="form-control specTariff${status.index}"
																	placeholder="Enter Price"
																	value="${dtoList.peakHourPrice}"> <label
																	class="error" style="display: none;"><small>This
																		field is required.</small></label>

															</div>
														</div>
													</div>

												</div>

											</div>
										</div>
									</div>


									<!-- perday price starts -->

									<div id="perDayPricing${status.index}"
										class="full-tab-container specialTariff" style="display: none">

										<div class="main-tab-content tab-content">

											<div id="specialTariff" class="tab-pane active"
												class="specialTariff">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group" id="regularTariff">
															<div class="col-md-4">
																<c:if test="${dtoList.vatInclusivePerDayPrice ne false}">

																	<input
																		name="carModelWithPricing[${status.index}].vatInclusivePerDayPrice"
																		type="checkbox" class="i-min-check " checked="checked"
																		id="vatInclusivePerDayPrice${status.index}">
																	<font size="3" color=grey> VAT (Select If
																		Inclusive)</font>
																</c:if>


																<c:if test="${dtoList.vatInclusivePerDayPrice ne true}">
																	<input
																		name="carModelWithPricing[${status.index}].vatInclusivePerDayPrice"
																		type="checkbox" class="i-min-check "
																		id="vatInclusivePerDayPrice${status.index}">
																	<font size="3" color=grey> VAT (Select If
																		Inclusive)</font>
																</c:if>


															</div>
															<div class="col-md-4"></div>
															<div class="col-md-4"></div>
														</div>


													</div>
												</div>

												<div class="row">
													<div class="col-md-12">
														<div class="form-group" id="specialTariff"
															class="specialTariff">

															<div class="col-md-6">

																<font size="3" color="grey"> <label>MONDAY(INR)</label></font>
																<input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceMondayDayPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceMonday.dayPrice"
																	class="form-control specTariff"
																	placeholder="Enter Day Price" min="0"
																	value="${dtoList.perDayPriceMonday == null ? 0 : dtoList.perDayPriceMonday.dayPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> &nbsp&nbsp <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceMondayPeakPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceMonday.peakPrice"
																	class="form-control specTariff"
																	placeholder="Enter Peak Price" min="0"
																	value="${dtoList.perDayPriceMonday == null ? 0 : dtoList.perDayPriceMonday.peakPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label><br> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceMondaySalePrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceMonday.salePrice"
																	class="form-control specTariff"
																	placeholder="Enter Sale Price" min="0"
																	value="${dtoList.perDayPriceMonday == null ? 0 : dtoList.perDayPriceMonday.salePrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> <input type="hidden"
																	name="carModelWithPricing[${status.index}].perDayPriceMonday.day"
																	value="MONDAY">

															</div>


															<div class="col-md-6">

																<font size="3" color="grey"> <label>TUESDAY
																		(INR)</label></font> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceTuesdayDayPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceTuesday.dayPrice"
																	class="form-control specTariff"
																	placeholder="Enter Day Price" min="0"
																	value="${dtoList.perDayPriceTuesday == null ? 0 : dtoList.perDayPriceTuesday.dayPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> &nbsp&nbsp <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceTuesdayPeakPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceTuesday.peakPrice"
																	class="form-control specTariff"
																	placeholder="Enter Peak Price" min="0"
																	value="${dtoList.perDayPriceTuesday == null ? 0 : dtoList.perDayPriceTuesday.peakPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label><br> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceTuesdaySalePrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceTuesday.salePrice"
																	class="form-control specTariff"
																	placeholder="Enter Sale Price" min="0"
																	value="${dtoList.perDayPriceTuesday == null ? 0 : dtoList.perDayPriceTuesday.salePrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> <input type="hidden"
																	name="carModelWithPricing[${status.index}].perDayPriceTuesday.day"
																	value="TUESDAY">

															</div>

														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group" id="specialTariff"
															class="specialTariff">

															<div class="col-md-6">

																<font size="3" color="grey"> <label>WEDNUSDAY(INR)</label></font>
																<input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceWednusdayDayPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceWednusday.dayPrice"
																	class="form-control specTariff"
																	placeholder="Enter Day Price" min="0"
																	value="${dtoList.perDayPriceWednusday == null ? 0 : dtoList.perDayPriceWednusday.dayPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> &nbsp&nbsp <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceWednusdayPeakPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceWednusday.peakPrice"
																	class="form-control specTariff"
																	placeholder="Enter Peak Price" min="0"
																	value="${dtoList.perDayPriceWednusday == null ? 0 : dtoList.perDayPriceWednusday.peakPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label><br> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceWednusdaySalePrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceWednusday.salePrice"
																	class="form-control specTariff"
																	placeholder="Enter Sale Price" min="0"
																	value="${dtoList.perDayPriceWednusday == null ? 0 : dtoList.perDayPriceWednusday.salePrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> <input type="hidden"
																	name="carModelWithPricing[${status.index}].perDayPriceWednusday.day"
																	value="WEDNUSDAY">

															</div>


															<div class="col-md-6">

																<font size="3" color="grey"> <label>THURSDAY
																		(INR)</label></font> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceThursdayDayPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceThursday.dayPrice"
																	class="form-control specTariff"
																	placeholder="Enter Day Price" min="0"
																	value="${dtoList.perDayPriceThursday == null ? 0 : dtoList.perDayPriceThursday.dayPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> &nbsp&nbsp <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceThursdayPeakPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceThursday.peakPrice"
																	class="form-control specTariff"
																	placeholder="Enter Peak Price" min="0"
																	value="${dtoList.perDayPriceThursday == null ? 0 : dtoList.perDayPriceThursday.peakPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label><br> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceThursdaySalePrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceThursday.salePrice"
																	class="form-control specTariff"
																	placeholder="Enter Sale Price" min="0"
																	value="${dtoList.perDayPriceThursday == null ? 0 : dtoList.perDayPriceThursday.salePrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> <input type="hidden"
																	name="carModelWithPricing[${status.index}].perDayPriceThursday.day"
																	value="THURSDAY">

															</div>

														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group" id="specialTariff"
															class="specialTariff">

															<div class="col-md-6">

																<font size="3" color="grey"> <label>FIRDAY(INR)</label></font>
																<input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceFridayDayPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceFriday.dayPrice"
																	class="form-control specTariff"
																	placeholder="Enter Day Price" min="0"
																	value="${dtoList.perDayPriceFriday == null ? 0 : dtoList.perDayPriceFriday.dayPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> &nbsp&nbsp <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceFridayPeakPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceFriday.peakPrice"
																	class="form-control specTariff"
																	placeholder="Enter Peak Price" min="0"
																	value="${dtoList.perDayPriceFriday == null ? 0 : dtoList.perDayPriceFriday.peakPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label><br> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceFridaySalePrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceFriday.salePrice"
																	class="form-control specTariff"
																	placeholder="Enter Sale Price" min="0"
																	value="${dtoList.perDayPriceFriday == null ? 0 : dtoList.perDayPriceFriday.salePrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> <input type="hidden"
																	name="carModelWithPricing[${status.index}].perDayPriceFriday.day"
																	value="FRIDAY">

															</div>


															<div class="col-md-6">

																<font size="3" color="grey"> <label>SATURDAY
																		(INR)</label></font> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceSaturdayDayPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceSaturday.dayPrice"
																	class="form-control specTariff"
																	placeholder="Enter Day Price" min="0"
																	value="${dtoList.perDayPriceSaturday == null ? 0 : dtoList.perDayPriceSaturday.dayPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> &nbsp&nbsp <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceSaturdayPeakPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceSaturday.peakPrice"
																	class="form-control specTariff"
																	placeholder="Enter Peak Price" min="0"
																	value="${dtoList.perDayPriceSaturday == null ? 0 : dtoList.perDayPriceSaturday.peakPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label><br> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceSaturdaySalePrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceSaturday.salePrice"
																	class="form-control specTariff"
																	placeholder="Enter Sale Price" min="0"
																	value="${dtoList.perDayPriceSaturday == null ? 0 : dtoList.perDayPriceSaturday.salePrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> <input type="hidden"
																	name="carModelWithPricing[${status.index}].perDayPriceSaturday.day"
																	value="SATURDAY">

															</div>

														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group" id="specialTariff"
															class="specialTariff">

															<div class="col-md-6">

																<font size="3" color="grey"> <label>SUNDAY(INR)</label></font>
																<input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceSundayDayPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceSunday.dayPrice"
																	class="form-control specTariff"
																	placeholder="Enter Day Price" min="0"
																	value="${dtoList.perDayPriceSunday == null ? 0 : dtoList.perDayPriceSunday.dayPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> &nbsp&nbsp <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceSundayPeakPrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceSunday.peakPrice"
																	class="form-control specTariff"
																	placeholder="Enter Peak Price" min="0"
																	value="${dtoList.perDayPriceSunday == null ? 0 : dtoList.perDayPriceSunday.peakPrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label><br> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="perDayPriceSundaySalePrice${status.index}"
																	name="carModelWithPricing[${status.index}].perDayPriceSunday.salePrice"
																	class="form-control specTariff"
																	placeholder="Enter Sale Price" min="0"
																	value="${dtoList.perDayPriceSunday == null ? 0 : dtoList.perDayPriceSunday.salePrice}">
																<label class="error" style="display: none;"><small>This
																		field is required.</small></label> <input type="hidden"
																	name="carModelWithPricing[${status.index}].perDayPriceSunday.day"
																	value="SUNDAY">

															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>





									<!-- perday price ends -->

									<%--onclick="return validateSpecialTariff();" --%>
									<br>
									<hr>
									<div>
										<h3>Security Deposit</h3>
									</div>
									<c:if test="${vendorType != null}">

										<c:if test="${vendorType != 'PRODUCER'}">

											<div class="row">
												<div class="col-md-2">

													<c:if test="${securityAmountRangeTypes != null}">

														<select class="form-control  select-price-type  select2"
															name="carModelWithPricing[${status.index}].securityAmountRangeType"
															id="securityAmountRangeTypeDropDown${status.index}"
															onchange="notifyForRangeType('labelForRange${status.index}','securityAmountRangeTypeDropDown${status.index}');">

															<%-- <option value="${null}">Select Range Type</option> --%>

															<c:if test="${dtoList.securityAmountRangeType != null}">
																<option value="${dtoList.securityAmountRangeType}">${dtoList.securityAmountRangeType}</option>
															</c:if>

															<c:forEach items="${securityAmountRangeTypes}"
																var="securityAmountRanges">

																<c:if test="${dtoList.securityAmountRangeType == null}">
																	<option value="${securityAmountRanges}">${securityAmountRanges}</option>
																</c:if>

																<c:if test="${dtoList.securityAmountRangeType != null}">
																	<c:if
																		test="${dtoList.securityAmountRangeType != securityAmountRanges}">
																		<option value="${securityAmountRanges}">${securityAmountRanges}</option>
																	</c:if>
																</c:if>

															</c:forEach>

														</select>
													</c:if>

												</div>
											</div>
											<br>

											<div class="row">
												<div class="col-md-4"></div>
												<div class="col-md-4" id="errorMsgDiv${status.index}"
													style="color: red;"></div>
												<div class="col-md-4"></div>
											</div>
											<br>
											<div class="row">

												<div class="col-md-3">
													<label id="labelForRange${status.index}"></label><br>
													<label>Click To Add More<small>(at least
															one)</small></label> <input type="button" value="add"
														onclick="addSecurityAmountDiv('${status.index}','SecurityAmountGroup${status.index}')">
												</div>

												<div class="col-md-9">
													<div class="row">
														<div class="col-md-3">
															<label>Start Range </label>
														</div>
														<div class="col-md-3">
															<label style="margin-left: -24px;">End Range</label>
														</div>
														<div class="col-md-3">
															<label style="margin-left: -55px;">Security
																Deposit</label>
														</div>
													</div>

													<div class="row col-md-12 pricing-carModel"
														id="SecurityAmountGroup${status.index}">

														<c:if test="${dtoList.securityAmountRanges != NULL}">

															<c:forEach items="${dtoList.securityAmountRanges}"
																var="securityAmountRange" varStatus="theCount">

																<c:set var="countOfSDDiv" value="${countOfSDDiv+1 }"></c:set>

																<c:if test="${securityAmountRange != NULL}">

																	<div id="SecurityAmountDiv${countOfSDDiv}"
																		carmodelindex="carModelWithPricing[${status.index}]">
																		<input type="text" name="" placeholder="Start Range"
																			value="${securityAmountRange.startRange }">
																		&nbsp;&nbsp;-&nbsp;&nbsp; <input type="text" name=""
																			placeholder="End Range"
																			value="${securityAmountRange.endRange }">
																		&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name=""
																			placeholder="Security Amount"
																			value="${securityAmountRange.securityAmount }">
																		<button
																			onclick="removeDiv('#SecurityAmountDiv${countOfSDDiv}');">
																			Remove</button>
																		<br> <br>
																	</div>

																</c:if>

															</c:forEach>


														</c:if>

													</div>
												</div>
											</div>
											<br>
											<hr>
											<div class="row">
												<div class="col-md-12">
													<h3>Cashback Details</h3>
												</div>
											</div>
											<div class="row">

												<div class="col-md-12">

													<div class="col-md-3">
														<c:if test="${dtoList.cashback.cashbackApplied}">
															Cash Back Applied : <input type="checkbox"
																name="carModelWithPricing[${status.index}].cashback.cashbackApplied"
																checked="checked" id="cashbackApplied${status.index}"
																onchange="showHideCashbackDiv('${status.index}');">
														</c:if>
														<c:if test="${!dtoList.cashback.cashbackApplied}">
															Cash Back Applied : <input type="checkbox"
																name="carModelWithPricing[${status.index}].cashback.cashbackApplied"
																id="cashbackApplied${status.index}"
																onchange="showHideCashbackDiv('${status.index}');">
														</c:if>
													</div>
													<div class="col-md-9" id="cashBackDiv${status.index}"
														style="display: none;">
														<div class="col-md-3">
															<c:if test="${cashBackTypes != null}">
																<select class="form-control  select-price-type  select2"
																	name="carModelWithPricing[${status.index}].cashback.cashbackType">

																	<c:if test="${dtoList.cashback.cashbackType != null}">
																		<option value="${dtoList.cashback.cashbackType}">${dtoList.cashback.cashbackType}</option>
																	</c:if>

																	<c:forEach var="cashBack" items="${cashBackTypes}">
																		<c:if
																			test="${cashBack != dtoList.cashback.cashbackType}">
																			<option value="${cashBack}">${cashBack}</option>
																		</c:if>
																	</c:forEach>
																</select>
															</c:if>
														</div>

														<div class="col-md-3">
															<div class="col-md-6">
																<label style="text-align: right;">Duration :</label>
															</div>
															<div class="col-md-6">
																<input type="number" id="duration${status.index}"
																	name="carModelWithPricing[${status.index}].cashback.duration"
																	value="${dtoList.cashback == null ? 0 : dtoList.cashback.duration}"
																	placeholder="duration" style="width: 50px;">
															</div>
														</div>
														<div class="col-md-3">
															<div class="col-md-6">
																<label style="text-align: right;"> Amount :</label>
															</div>
															<div class="col-md-6">
																<input type="number" id="amount${status.index}"
																	name="carModelWithPricing[${status.index}].cashback.amount"
																	value="${dtoList.cashback == null ? 0 : dtoList.cashback.amount}"
																	placeholder="amount" style="width: 50px;">
															</div>
														</div>
														<div class="col-md-3">
															<div class="col-md-6">
																<label style="text-align: right;"> Max :</label>
															</div>
															<div class="col-md-6">
																<input type="number" id="max${status.index}"
																	name="carModelWithPricing[${status.index}].cashback.max"
																	value="${dtoList.cashback == null ? 0 : dtoList.cashback.max}"
																	style="width: 50px;">
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="cashbackErrorDiv"
												id="cashbackErrorDiv${status.index}"></div>
										</c:if>

										<br>
										<hr>
										<div class="row">
											<div class="col-md-12">
												<h3>Tax Details</h3>
											</div>
										</div>
										<div class="row">
											<div class="col-md-2">
												<label>Intra State Taxes</label>
											</div>
											<div class="col-md-2">
												<div class="col-md-6">
													<label>CGST</label>
												</div>
												<div class="col-md-6">
													<input type="text" id="intraStateCGST${status.index}"
														value="${dtoList.taxes == null ? 0 : dtoList.taxes.intraState.cgst}"
														name="carModelWithPricing[${status.index}].taxes.intraState.cgst"
														style="width: 100px;">
												</div>
											</div>
											<div class="col-md-2">
												<div class="col-md-6">
													<label>SGST</label>
												</div>
												<div class="col-md-6">
													<input type="number" id="intraStateSGST${status.index}"
														value="${dtoList.taxes == null ? 0 : dtoList.taxes.intraState.sgst}"
														name="carModelWithPricing[${status.index}].taxes.intraState.sgst"
														style="width: 100px;">
												</div>
											</div>
											<div class="col-md-2">
												<div class="col-md-6">
													<label>CESS</label>
												</div>
												<div class="col-md-6">
													<input type="number" id="intraStateCESS${status.index}"
														value="${dtoList.taxes == null ? 0 : dtoList.taxes.intraState.cess}"
														name="carModelWithPricing[${status.index}].taxes.intraState.cess"
														style="width: 100px;">
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-2">
												<label>Inter State Taxes</label>
											</div>
											<div class="col-md-2">
												<div class="col-md-6">
													<label>IGST</label>
												</div>
												<div class="col-md-6">
													<input type="number" id="interStateIGST${status.index}"
														value="${dtoList.taxes == null ? 0 : dtoList.taxes.interState.igst}"
														name="carModelWithPricing[${status.index}].taxes.interState.igst"
														style="width: 100px;">
												</div>
											</div>
									</c:if>
									<!-- Partial Payment Div starts -->

									<br>
									<hr>
									<div>
										<h3>Voler Weekly Installment</h3>
									</div>
									<c:if test="${vendorType != null}">

										<c:if test="${vendorType != 'PRODUCER'}">

											<div class="row">
												<div class="col-md-4">


													<c:choose>
														<c:when
															test="${dtoList.volerWeeklyInstallmentModel.volerWeeklyInstallmentEnabled eq true }">
															<input
																name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.volerWeeklyInstallmentEnabled"
																type="checkbox" id="partialPaymentStatus"
																checked="checked">
															<font size="3" color=grey><small>Enable
																	Voler Weekly Installment </small> </font>


														</c:when>
														<c:otherwise>

															<input
																name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.volerWeeklyInstallmentEnabled"
																type="checkbox" id="partialPaymentStatus">
															<font size="3" color=grey><small>Enable
																	Voler Weekly Installment </small> </font>
														</c:otherwise>
													</c:choose>




												</div>
												<div class="col-md-4"></div>
												<div class="col-md-4"></div>
											</div>
											<br>

											<div id="partialPaymentDiv" style="display: none">
												<div class="row">

													<div class="col-md-12">
														<div class="form-group" id="bookingEligibility"
															class="bookingEligibility">

															<div class="col-md-4">
																<font size="3" color=grey><label>Booking
																		Date Eligibility (Mins)</label></font> <input type="number"
																	onkeypress="return isNumberKey(event)"
																	id="bookingDateEligibility${status.index}"
																	name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.bookingDateEligibility"
																	value="${dtoList.volerWeeklyInstallmentModel.bookingDateEligibility == null ? 0 : dtoList.volerWeeklyInstallmentModel.bookingDateEligibility}"
																	placeholder="Enter in Minutes" min="0"></input> <label
																	class="error" style="display: none;"><small>This
																		field is required.</small></label>
															</div>


															<div class="col-md-4">
																<div class="col-md-6">
																	<font size="3" color=grey><label>Booking
																			Duration Eligibility (Mins)</label></font>
																	<c:choose>
																		<c:when
																			test="${dtoList.volerWeeklyInstallmentModel.bookingDurationVariant eq true }">
																			<input
																				name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.bookingDurationVariant"
																				type="checkbox" checked="checked"
																				id="bookingDurationEligibility">


																		</c:when>
																		<c:otherwise>
																			<input
																				name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.bookingDurationVariant"
																				type="checkbox" id="bookingDurationEligibility">
																		</c:otherwise>
																	</c:choose>
																</div>

																<br> <br>
																<div class="col-md-6" style="display: none"
																	id="bookingDurationEligibilityDiv">
																	<input type="number"
																		onkeypress="return isNumberKey(event)"
																		value="${dtoList.volerWeeklyInstallmentModel.bookingDurationEligiblity == null ? 0 : dtoList.volerWeeklyInstallmentModel.bookingDurationEligiblity}"
																		name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.bookingDurationEligiblity"
																		id="bookingDurationEligibility${status.index}"
																		placeholder="Enter in Minutes" min="0"> <label
																		class="error" style="display: none;"><small>This
																			field is required.</small></label>
																</div>
															</div>
															<div class="col-md-4">

																<div class="col-md-6">
																	<font size="3" color=grey><label>Booking
																			Price Eligibility (INR)</label></font>
																	<c:choose>
																		<c:when
																			test="${dtoList.volerWeeklyInstallmentModel.bookingPriceVariant eq true }">
																			<input
																				name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.bookingPriceVariant"
																				type="checkbox" checked="checked"
																				id="bookingPriceEligibility">

																		</c:when>
																		<c:otherwise>
																			<input
																				name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.bookingPriceVariant"
																				type="checkbox" id="bookingPriceEligibility">
																		</c:otherwise>
																	</c:choose>
																</div>
																<br> <br>
																<div class="col-md-6" style="display: none"
																	id="bookingPriceEligibilityDiv">
																	<input type="number"
																		onkeypress="return isNumberKey(event)"
																		id="bookingPriceEligibility${status.index}"
																		name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.bookingPriceEligibility"
																		value="${dtoList.volerWeeklyInstallmentModel.bookingPriceEligibility==null ? 0 : dtoList.volerWeeklyInstallmentModel.bookingPriceEligibility}"
																		class="form-control regTariff${status.index}"
																		placeholder="Enter Price" min="0"> <label
																		class="error" style="display: none;"><small>This
																			field is required.</small></label>

																</div>
															</div>

														</div>
													</div>


												</div>

												<br>

												<div class="row">
													<div class="col-md-8">
														<label class=" col-md-4 text-right pricing-label"
															style="color: gray">Select Installment Variant </label>
														<div class="col-md-4">
															<select id="installment${status.index}"
																name="carModelWithPricing[${status.index}].volerWeeklyInstallmentModel.installmentRule"
																class="form-control">


																<%-- <option value="${dtoList.pricingType}"
																	id="selectedValue${status.index}"
																	style="pointer-events: none; cursor: default;" selected
																	value="${dtoList.pricingType}">${dtoList.pricingType}</option> --%>
																<c:forEach items="${installmentRules}" var="installment">


																	<option value="${installment}">${installment}</option>


																</c:forEach>

															</select>

														</div>


														<div class="col-md-4"></div>

													</div>
													<div class="col-md-6"></div>

												</div>


												<div class="row">
													<div class="col-md-4"></div>
													<div class="col-md-4" id="errorMsgDiv${status.index}"
														style="color: red;"></div>
													<div class="col-md-4"></div>
												</div>
												<br>
												<div class="row">

													<div class="col-md-6">
														<label id="partialPayment${status.index}"></label><br>
														<label>Click To Add Voler Weekly Installment Rules</label>
														<input type="button" value="add"
															onclick="addPartialPaymentDiv('${status.index}','partialPaymentGroup${status.index}')">
													</div>

													<div class="col-md-10">
														<div class="row">

															<div class="col-md-2">
																<label style="margin-left: 3px;">Payment
																	Percentage</label>
															</div>
															<div class="col-md-2">
																<label style="margin-left: 23px;">Duration
																	Before Booking</label>
															</div>
															<div class="col-md-2">
																<label style="margin-left: 43px;">Flexible
																	Installment Duration</label>
															</div>
															<div class="col-md-2">
																<label style="margin-left: 68px;">Refund
																	Percentage</label>
															</div>
															<div class="col-md-2">
																<label style="margin-left: 110px;">Select Refund
																	Variant</label>
															</div>
														</div>

														<div class="row col-md-12 volerWeeklyInstallment"
															id="partialPaymentGroup${status.index}">
															<c:if
																test="${dtoList.volerWeeklyInstallmentModel.volerWeeklyInstallmentRules != NULL}">

																<c:forEach
																	items="${dtoList.volerWeeklyInstallmentModel.volerWeeklyInstallmentRules}"
																	var="partialPaymentRule" varStatus="count">

																	<c:set var="countOfPartialPaymentDiv"
																		value="${countOfPartialPaymentDiv+1 }"></c:set>

																	<c:if test="${partialPaymentRule != NULL}">

																		<div id="PartialPaymentDiv${countOfPartialPaymentDiv}"
																			carmodelindex="carModelWithPricing[${status.index}]">

																			<input type="text" name=""
																				placeholder="Payment Percentage"
																				value="${partialPaymentRule.percentagePaymentAtEachLevel}">
																			&nbsp;&nbsp;<input type="text" name=""
																				placeholder="Payment Time"
																				value="${partialPaymentRule.timeForPercentagePayment }">
																			&nbsp;&nbsp;<input type="text" name="" placeholder=""
																				value="${partialPaymentRule.flexiblePaymentTime }">
																			&nbsp;&nbsp; <input type="text" name=""
																				placeholder="Security Amount"
																				value="${partialPaymentRule.refundPercentage }">

																			<select class="form-control" name=""
																				style="width: 170px; height: 31px; float: right;">
																				<option value="${partialPaymentRule.refundVariant}"
																					style="pointer-events: none; cursor: default;"
																					selected>${partialPaymentRule.refundVariant}</option>
																				<c:forEach items="${refundVariant}"
																					var="refundVariant">
																					<%-- --%>
																					<c:if
																						test="${partialPaymentRule.refundVariant != refundVariant}">
																						<option value="${refundVariant}">${refundVariant}</option>
																					</c:if>

																				</c:forEach>





																			</select>

																			<button style="margin-top: 15px;"
																				onclick="removePartialPaymentDiv('#PartialPaymentDiv${countOfPartialPaymentDiv}');">
																				Remove</button>
																			<br> <br>
																		</div>

																	</c:if>

																</c:forEach>


															</c:if>

														</div>
													</div>
												</div>

											</div>
										</c:if>


									</c:if>






									<!-- Partial Payment Div ends -->


								</div>
							</div>
						</div>
					</c:forEach>
					<div>


						<div class="col-md-2">
							<div class="col-md-6">
								<label>CESS</label>
							</div>
							<div class="col-md-6">
								<input type="number" id="interStateCESS${status.index}"
									value="${dtoList.taxes == null ? 0 : dtoList.taxes.interState.cess}"
									name="carModelWithPricing[${status.index}].taxes.interState.cess"
									style="width: 100px;">
							</div>
						</div>
					</div>
					<br>
					<div class="taxesErrorDiv" id="taxesErrorDiv${status.index}"></div>

					<br>
					<hr>
					<br>

					<div class="row">
						<div class="col-md-12">
							<h3>Car Left Details</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<div class="col-md-3">
									<span>Car Left Functionality Applied : <c:if
											test="${dtoList.carLeftDeatails.carLeftApplicable}">
											<input type="checkbox"
												name="carModelWithPricing[${status.index}].carLeftDeatails.carLeftApplicable"
												id="carLeftApplicable${status.index}"
												onclick="showHideCarLeftDiv('${status.index}');"
												checked="checked">
										</c:if> <c:if test="${!dtoList.carLeftDeatails.carLeftApplicable}">
											<input type="checkbox"
												name="carModelWithPricing[${status.index}].carLeftDeatails.carLeftApplicable"
												id="carLeftApplicable${status.index}"
												onclick="showHideCarLeftDiv('${status.index}');">
										</c:if>
									</span>
								</div>
							</div>
						</div>
					</div>

					<c:if test="${dtoList.carLeftDeatails.carLeftApplicable}">
						<div class="car-left-div" id="carLeftDiv${status.index}">
					</c:if>
					<c:if test="${!dtoList.carLeftDeatails.carLeftApplicable}">
						<div class="car-left-div" id="carLeftDiv${status.index}"
							style="display: none;">
					</c:if>
					<div class="row">
						<div class="col-md-12">

							<div class="col-md-3">
								<div class="col-md-6">
									<b>Minimum Cars</b> :
								</div>

								<div class="col-md-6">
									<input type="number" class="setWidth"
										name="carModelWithPricing[${status.index}].carLeftDeatails.minimumCars"
										id="minimumCars${status.index}"
										value="${dtoList.carLeftDeatails != null ? dtoList.carLeftDeatails.minimumCars : 0}">
								</div>
							</div>

							<c:if test="${dtoList.carTypeCounter != null}">
								<div class="col-md-3">
									<div class="col-md-6">
										<b>Total Live Cars</b> :
									</div>

									<div class="col-md-6">
										<span> Real Cars :- ${dtoList.carTypeCounter.realCars}</span>
										&nbsp;&nbsp;&nbsp;<span> Alias Cars :-
											${dtoList.carTypeCounter.aliasCars}</span> <input type="hidden"
											id="liveCarHiddenField${status.index}"
											value="${dtoList.carTypeCounter.realCars + dtoList.carTypeCounter.aliasCars}">
									</div>
								</div>
							</c:if>
						</div>
					</div>

					<br> <br>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-3">
								<b>Click To Add More</b><small>(at least one)</small><input
									type="button"
									onclick="addCarLeftDiv('${status.index}','carLeftRanges${status.index}')"
									value="Add">
							</div>

							<div class="col-md-9">
								<div class="row">
									<div class="col-md-3">
										<label>Car Left Percentage</label>
									</div>
									<div class="col-md-3">
										<label>Notification</label>
									</div>
								</div>

								<div class="voler-car-left">
									<div id="carLeftRanges${status.index}"
										carleftrange="carModelWithPricing[${status.index}]">
										<c:if
											test="${dtoList.carLeftDeatails.carLeftNotifiations != null}">
											<c:forEach var="notifiations"
												items="${dtoList.carLeftDeatails.carLeftNotifiations}">

												<c:set var="counterOfCarLeftDiv"
													value="${counterOfCarLeftDiv + 1}"></c:set>
												<div id="CarLeft${counterOfCarLeftDiv}">
													<div class="row">
														<div class="col-md-3">
															<input type="number" name=""
																value="${notifiations != null ? notifiations.availabilityPercentage : 0}">
														</div>
														<div class="col-md-3">
															<input type="text" name="" value="${notifiations.msg}">
														</div>
														<div class="col-md-3">
															<select class="form-control">
																<c:forEach var="carLeftColours"
																	items="${coloursForCarLeft}">
																	<c:if test="${carLeftColours == notifiations.colour}">
																		<option value="${carLeftColours}" selected="selected">${carLeftColours}</option>
																	</c:if>
																	<c:if test="${carLeftColours != notifiations.colour}">
																		<option value="${carLeftColours}">${carLeftColours}</option>
																	</c:if>
																</c:forEach>
															</select>
														</div>
														<div class="col-md-3">
															<button
																onclick="removeDiv('#CarLeft${counterOfCarLeftDiv}')">
																remove</button>
														</div>
													</div>
													<br>
												</div>

											</c:forEach>
										</c:if>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="errorMsgDivOfCarLeft${status.index}"></div>
			</div>
			<br>
			<hr>
			<br>

			<div>
				<h3>Voler Deals Details</h3>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4">
						VDeal Applied :
						<c:if test="${dtoList.vDeals.vDealApplied}">
							<input type="checkbox" checked="checked"
								name="carModelWithPricing[${status.index}].vDeals.vDealApplied"
								id="vDealApplied${status.index}"
								onclick="showHideVdealsDiv('${status.index}');">
						</c:if>
						<c:if test="${!dtoList.vDeals.vDealApplied}">
							<input type="checkbox"
								name="carModelWithPricing[${status.index}].vDeals.vDealApplied"
								id="vDealApplied${status.index}"
								onclick="showHideVdealsDiv('${status.index}');">
						</c:if>
					</div>

					<div class="col-md-4">Minimum Minutes Of Booking :-
						${minimumMinsOfBooking}</div>
					<div class="col-md-4">
						<b style="color: red">Kindly mark all deals available in
							Database as Archive , If you are disabling the VDeal Feature on
							this car model.</b>
					</div>
				</div>
			</div>
			<br> <br>
			<c:if test="${dtoList.vDeals.vDealApplied}">
				<div class="vDealsDiv" id="vDealsDetails${status.index}">
			</c:if>

			<c:if test="${!dtoList.vDeals.vDealApplied}">
				<div class="vDealsDiv" id="vDealsDetails${status.index}"
					style="display: none;">
			</c:if>
			<div class="row">
				<div class="col-md-12">

					<div class="col-md-3">
						<div class="col-md-6">
							<b>Min Hole Size</b><small>(In Minutes)</small> :
						</div>

						<div class="col-md-6">
							<input type="number" class="setWidth"
								name="carModelWithPricing[${status.index}].vDeals.minHoleSize"
								id="minHoleSize${status.index}"
								value="${dtoList.vDeals != null ? dtoList.vDeals.minHoleSize : 0}"
								required="required">
						</div>
					</div>

					<div class="col-md-3">
						<div class="col-md-6">
							<b>Max Hole Size</b><small>(In Minutes)</small> :
						</div>

						<div class="col-md-6">
							<input type="number" class="setWidth"
								name="carModelWithPricing[${status.index}].vDeals.maxHoleSize"
								id="maxHoleSize${status.index}"
								value="${dtoList.vDeals != null ? dtoList.vDeals.maxHoleSize : 0}"
								required="required">
						</div>
					</div>

					<div class="col-md-3">
						<div class="col-md-6">
							<b>Max Days</b><small></small> :
						</div>

						<div class="col-md-6">
							<input type="number" class="setWidth"
								name="carModelWithPricing[${status.index}].vDeals.maxDays"
								id="maxDays${status.index}"
								value="${dtoList.vDeals != null ? dtoList.vDeals.maxDays : 0}"
								required="required">
						</div>
					</div>

					<div class="col-md-3">
						<div class="col-md-6">
							<b>Security Deposite</b><small></small> :
						</div>

						<div class="col-md-6">
							<input type="number" class="setWidth"
								name="carModelWithPricing[${status.index}].vDeals.securityDeposite"
								id="securityDeposite${status.index}"
								value="${dtoList.vDeals != null ? dtoList.vDeals.securityDeposite : 0}"
								required="required">
						</div>
					</div>
				</div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-3">
						<b>Click To Add More</b><small>(at least one)</small><input
							type="button"
							onclick="addVDealsOfferDiv('${status.index}','vDealsRanges${status.index}')"
							value="Add Deals">
					</div>

					<div class="col-md-8">
						<div class="row">
							<div class="col-md-3">
								<label>Minimum Minutes</label>
							</div>
							<div class="col-md-3">
								<label>Offer Discount</label>
							</div>
							<div class="col-md-3">
								<label>Max Discount</label>
							</div>
						</div>

						<div class="voler-deals">
							<div id="vDealsRanges${status.index}"
								dealsrange="carModelWithPricing[${status.index}]">
								<c:if test="${dtoList.vDeals.vDealsOffers != null}">
									<c:forEach var="offers" items="${dtoList.vDeals.vDealsOffers}"
										varStatus="vDealsStatus">
										<c:set var="counterOfVDealsDiv"
											value="${counterOfVDealsDiv + 1}"></c:set>
										<div id="VDeals${counterOfVDealsDiv}">
											<div class="row">
												<div class="col-md-3">
													<input type="number" name=""
														value="${offers != null ? offers.minutes : 0}">
												</div>
												<div class="col-md-3">
													<input type="number" name=""
														value="${offers != null ? offers.discount : 0}">
												</div>
												<div class="col-md-3">
													<input type="number" name=""
														value="${offers != null ? offers.maxDiscount : 0}">
												</div>
												<div class="col-md-3">
													<button onclick="removeDiv('#VDeals${counterOfVDealsDiv}')">
														Remove</button>
												</div>
											</div>
											<br>
										</div>

									</c:forEach>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="errorMsgDivOfVdeals${status.index}"></div>
		</div>

	</div>

	<div>
		<div>&nbsp;</div>
		<div>&nbsp;</div>

		<button class="btn btn-default btn-primary pull-right"
			id="configurationalDetails" onClick="return validatePricingType();"
			type="submit">Preview &amp; Submit</button>


	</div>

	</form>
	<div class="row" id="loader" style="display: none">
		<div class="col-lg-6 text-left"
			style="position: relative; right: 80px"></div>
		<div class="col-lg-6 text-right"
			style="position: relative; right: 80px" id="loader">


			<img style="float: right" alt="model"
				src="/admin/resources/dist/img/loader.gif" class="thumbnail">
		</div>
	</div>
	<div>
		<c:if test="${isProcessPending != false}">
			<button class="btn btn-default btn-primary pull-left"
				onclick="getVendorId();" type="submit">Back</button>
		</c:if>

	</div>
	<div>&nbsp;</div>
	<div>&nbsp;</div>

	</div>


	<!-- Create Model Modal start-->
	<div class="modal fade" id="createModel" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Create Car Model</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
						<div class="form-group">
							<label class=" col-md-3 control-label">Brand</label>
							<div class="col-md-6">
								<input type="text" class="form-control"
									placeholder="Enter Brand Name" />
							</div>
						</div>

						<div class="form-group">
							<label class=" col-md-3 control-label">Category</label>
							<div class="col-md-6">
								<input type="text" class="form-control"
									placeholder="Enter Brand Name" />
							</div>
						</div>

						<div class="form-group">
							<label class=" col-md-3 control-label">Model</label>
							<div class="col-md-6">
								<input type="text" class="form-control"
									placeholder="Enter Brand Name" />
							</div>
						</div>

						<div class="form-group">
							<label class=" col-md-3 control-label">Varient</label>
							<div class="col-md-6">
								<input type="text" class="form-control"
									placeholder="Enter Brand Name" />
							</div>
						</div>

						<div class="form-group">
							<label class=" col-md-3 control-label">Upload Image</label>
							<div class="col-md-6">
								<input type="file" class="filestyle"
									data-iconname="fa-cloud-upload" data-buttonname="btn-warning">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Create Model Modal end-->
	<!-- <div class="modal-footer">
						<button class="btn btn-success" type="button"
							data-bb-handler="success">Update</button>
					</div -->
	</div>
	</div>
	</div>
	<tiles:insertAttribute name="footer" />


	<script type="text/javascript">
  function checkDiscount(val){
   var element=document.getElementById('discountPercentage');
   if(val=='All')
     element.style.display='block';
   else  
     element.style.display='none';
  }
  
  function isNumberKey(event)
  {
     var charCode = (event.which) ? event.which : event.keyCode;
     if (charCode < 48 || charCode > 57)
             {
                      return false;
             }
     else{    
   	  $(".error").hide();
             return true;
     }  
  }
	</script>

	<script>
 function validatePricingType(){
	 $("#loader").show();
	 
	 var numberOfModels=$("#numberOfModels").val();
	 for ( i=0;i<numberOfModels;i++){
		 
		var priceTypeChecker=$("#select"+i).val();
		if(priceTypeChecker ==null || priceTypeChecker==""){
			alert("Please Select the Pricing type for each CarModel");
			 $("#loader").hide();
		 return false;
		}
		if(priceTypeChecker == 'PER_HOUR'){
		 if($("#peakSeasonTariff"+i).val()==0 || $("#weekendTariff"+i).val()==0 || $("#pricePerHour"+i).val()==0){
		
			 alert("All Fields are mandatory and values must be greater than zero");
				 $("#loader").hide();
			 return false;
				 }
		}
		if(priceTypeChecker == 'FIXED'){
		 if($("#dailyTariff"+i).val()==0||$("#monthlyTariff"+i).val()==0 || $("#hourlyTariff"+i).val()==0){
				 alert("All Fields are mandatory and values must be greater than zero");
				 $("#loader").hide();
				 return false;
			 }
		}
        if(priceTypeChecker =='DISCOUNT'){
		  if($("#discountInPercentage"+i).val()==0){
			
					 alert("This field is mandatory");
					 $("#loader").hide();
					 return false;
				 }
			 
		 }
        
        if($("#bookingDateEligibility"+i).val()==""|| $("#bookingDateEligibility"+i).val()==null){
        	document.getElementById("bookingDateEligibility"+i).value = 0;
        }
        if($("#bookingDurationEligibility"+i).val()==""|| $("#bookingDurationEligibility"+i).val()==null){
        	document.getElementById("bookingDurationEligibility"+i).value = 0;
        }
        if($("#bookingPriceEligibility"+i).val()==""|| $("#bookingPriceEligibility"+i).val()==null){
        	document.getElementById("bookingPriceEligibility"+i).value = 0;
        }
        
        formNeedToSubmit = true;
	 
	 }
	 
	 
	 var keywords = ["startRange","endRange","securityAmount"];

	 $('.pricing-carModel').each(function() {
	 
		var count =0;
	    $( this ).children().each(function() {
	 	
	    	var carModelIndex=($(this).attr("carmodelindex"));
	 		var inputCount = 0;

	 		$( this ).children().each(function() {

	 			/* var isButton = ($(this) == $('input[type="button"]'));
	 			$(this).find('input[type="button"]'); */
	 			
	 			if($(this).is('input')){
	 				
	 				if(inputCount ==3){
		 
		 				inputCount=0;
		 			}
		
	 				// 0 , 1 ,2
		 			$(this).attr("name",carModelIndex+".securityAmountRanges["+count+"]."+keywords[inputCount]);
	
		 			inputCount ++;
	 			
	 		}
	 	});
	 	count++;
	 });
	});
	 
	 var keywordsOfVDeals = ["minutes","discount","maxDiscount"];
	 $('.voler-deals').each(function() {
		 
			var count =0;
		    $( this ).children().each(function() {
		 	
		    	var dealsrange=($(this).attr("dealsrange"));
		 		var inputCount = 0;

		 		$( this ).children().each(function() {
		 			
		 			if($(this).is('div')){
		 				
		 				$( this ).children().each(function() {
		 					
		 					
		 					if($(this).is('div')){
		 						
		 						$( this ).children().each(function() {
		 							
		 							if($(this).is('div')){
		 								
		 								$( this ).children().each(function() {
		 									
		 									if($(this).is('input')){
								 				
								 				if(inputCount ==3){
									 
									 				inputCount=0;
									 			}
											
								 				$(this).attr("name",dealsrange+".vDeals.vDealsOffers["+count+"]."+keywordsOfVDeals[inputCount]);
								
												inputCount ++;
								 			
								 			}
		 								});
		 							}
		 						});
		 					}
		 					
		 				});
		 			}
		 			count++;
		 	});
		 	
		 });
		});
	 
	 var keywordsOfCarLeft = ["availabilityPercentage","msg","colour"];
	 $('.voler-car-left').each(function() {
		 
			var count = 0;
		    $( this ).children().each(function() {
		 	
		    	var carleftrange=($(this).attr("carleftrange"));
		 		var inputCount = 0;

		 		$( this ).children().each(function() {
		 			
		 			if($(this).is('div')){
		 				
		 				$( this ).children().each(function() {
		 					
		 					
		 					if($(this).is('div')){
		 						
		 						$( this ).children().each(function() {
		 							
		 							if($(this).is('div')){
		 								
		 								$( this ).children().each(function() {
		 									
		 									if($(this).is('input')){
								 				
								 				if(inputCount ==3){
									 
									 				inputCount=0;
									 			}
											
								 				$(this).attr("name",carleftrange+".carLeftDeatails.carLeftNotifiations["+count+"]."+keywordsOfCarLeft[inputCount]);
								
												inputCount ++;
								 			
								 			}else if($(this).is('select')){
								 				
								 				if(inputCount ==3){
													 
									 				inputCount=0;
									 			}
								 				
								 				$(this).attr("name",carleftrange+".carLeftDeatails.carLeftNotifiations["+count+"]."+keywordsOfCarLeft[inputCount]);
												
												inputCount ++;
								 			}
		 								});
		 							}
		 						});
		 					}
		 					
		 				});
		 			}
		 			count++;
		 	});
		 	
		 });
		});

	 var vwi = ["percentagePaymentAtEachLevel","timeForPercentagePayment","flexiblePaymentTime","refundPercentage","refundVariant"];

	 $('.volerWeeklyInstallment').each(function() {
	 
		var count =0;
	    $( this ).children().each(function() {
	 	
	    	var carModelIndex=($(this).attr("carmodelindex"));
	 		var inputCount = 0;

	 		$( this ).children().each(function() {

	 		
	 			
	 			if($(this).is('input')){
	 				
	 				if(inputCount ==5){
		 
		 				inputCount=0;
		 			}
		
	 				// 0 , 1 ,2,3,4,5
		 			$(this).attr("name",carModelIndex+".volerWeeklyInstallmentModel.volerWeeklyInstallmentRules["+count+"]."+vwi[inputCount]);
	
		 			inputCount ++;
	 			
	 		}
	 			else if($(this).is('select')){
	 				if(inputCount ==5){
	 					 
		 				inputCount=0;
		 			}
	 				$(this).attr("name",carModelIndex+".volerWeeklyInstallmentModel.volerWeeklyInstallmentRules["+count+"]."+vwi[inputCount]);
	 				inputCount ++;	
	 			}
	 	});
	 	count++;
	 });
	});
	 
	 
	 
	 
	 
	 
	 
	

	 
	return true; 
 }
 
 
	 function validateSecurityAmountRange(){
		
		 var successStatus = true;

		 var counterForErrorMsgDiv = 0;
		 $("#loader").show();
		 $('.pricing-carModel').each(function() {
		 
			var securityRangeArray = [];
			var count =0;
			var securityRangeData = new Array(3);
		    $( this ).children().each(function() {
		 	
		    	var carModelIndex=($(this).attr("carmodelindex"));
		 		var inputCount = 0;

		 		
		 		$( this ).children().each(function() {
		 			
		 			if($(this).is('input')){
		 				
		 				if(inputCount ==3){
			 
			 				inputCount=0;
			 			}
			
		 				securityRangeData[inputCount] = $(this).val(); 
			 			inputCount ++;
		 		}
		 	});
		 		securityRangeArray = securityRangeArray.concat(securityRangeData);
		 		
		 	count++;
		 });
		    
		    document.getElementById('errorMsgDiv'+counterForErrorMsgDiv).innerHTML = "";
		    
		    if(securityRangeArray.length > 0){
		    
			    if(parseInt(securityRangeArray[0]) != 0){
			    	
			    	document.getElementById('errorMsgDiv'+counterForErrorMsgDiv).innerHTML = "Initaila start range should be ZERO.";
			    	successStatus =  false;
			    	$("#loader").hide();
		    		return successStatus;
			    }else if(securityRangeArray[securityRangeArray.length-2] != '*'){
			    
			    	document.getElementById('errorMsgDiv'+counterForErrorMsgDiv).innerHTML = "Last end range should be '*'.";
			    	successStatus =  false;
			    	$("#loader").hide();
		    		return successStatus;
			    }
			    
			    
			    var i = 0;
			    for(i = 0; i < securityRangeArray.length;){
			    	
			    	var j = i + 1;
			    	var k = j + 2;
			    	
			    	if(k >= securityRangeArray.length)
			    		break;
			    	
			    	if(securityRangeArray[k] != '*' || securityRangeArray[j] != '*'){
			    		
				    	if(parseInt(securityRangeArray[j]) != parseInt(securityRangeArray[k])){
				    		
				    		document.getElementById('errorMsgDiv'+counterForErrorMsgDiv).innerHTML = "Security amount range is not proper.";
				    		successStatus =  false;
				    		$("#loader").hide();
				    		return successStatus;
				    	}
			    	}
			    	
			    	i = i + 3;
			    }
			    
			    }else{
			    	document.getElementById('errorMsgDiv'+counterForErrorMsgDiv).innerHTML = "Security amount is required.";
			    	successStatus =  false;
			    	$("#loader").hide();
		    		return successStatus;
		    }
		    counterForErrorMsgDiv++;
		});
		return successStatus;
	 }
	 
	 
	 function validateVDeal(){
		 
		 var numberOfModels=$("#numberOfModels").val();
		 var successStatus = true;
		 var counterForErrorMsgDiv = 0;
		 
		 for(var i = 0; i < numberOfModels; i++){
		
			 var vdealApplied = document.getElementById("vDealApplied"+i).checked;
			 
			 if(vdealApplied){
				 
			 
				 document.getElementById('errorMsgDivOfVdeals'+i).innerHTML = "";
				 
				 var minimumMinsOfBooking = document.getElementById('minimumMinsOfBooking').value;
				 var minHS = document.getElementById('minHoleSize'+i).value;
				 var maxHS = document.getElementById('maxHoleSize'+i).value;
				 var maximumDays = document.getElementById('maxDays'+i).value;
				 
				var minHoleSize = 0;
				var maxHoleSize = 0;
				var maxDays = 0;
				var minBookingDuration = 0;
				
				if(minHS != null && minHS != ""){
					minHoleSize = parseInt(minHS);
				}
				
				if(maxHS != null && maxHS != ""){
					maxHoleSize = parseInt(maxHS);
				}
				
				if(maximumDays != null && maximumDays != ""){
					maxDays = parseInt(maximumDays);
				}
				
				if(minimumMinsOfBooking != null && minimumMinsOfBooking != ""){
					
					minBookingDuration = parseInt(minimumMinsOfBooking);
				}
				
				if(minHoleSize < minBookingDuration){
					
					$("#loader").hide();
					document.getElementById('errorMsgDivOfVdeals'+i).innerHTML = "<font color='red'> Minimum hole size should be greter or equal to minimum minutes of booking.</font>";
					return false;
				}
				
				if(minHoleSize <= 0){
					 
					$("#loader").hide();
					 document.getElementById('errorMsgDivOfVdeals'+i).innerHTML = "<font color='red'> Please enter valid min hole hize.</font>";
					 return false;
				}
				 
				if(maxHoleSize <= 0){
					 
					$("#loader").hide();
					document.getElementById('errorMsgDivOfVdeals'+i).innerHTML = "<font color='red'> Please enter valid max hole size.</font>";
					return false;
				}
				 
				if(maxDays <= 0){
					 
					$("#loader").hide();
					document.getElementById('errorMsgDivOfVdeals'+i).innerHTML = "<font color='red'> Please enter valid max days.</font>";
					return false;
				}
				
				if(minHoleSize % 30 != 0){
					 
					$("#loader").hide();
					document.getElementById('errorMsgDivOfVdeals'+i).innerHTML = "<font color='red'> Min hole size should be multiple of 30.</font>";
					return false;
				}
				 
				if(maxHoleSize % 30 != 0){
					 
					$("#loader").hide();
					document.getElementById('errorMsgDivOfVdeals'+i).innerHTML = "<font color='red'> Max hole size should be multiple of 30.</font>";
					return false;
				}
				
				if(maxHoleSize < minHoleSize){
					
					$("#loader").hide();
					document.getElementById('errorMsgDivOfVdeals'+i).innerHTML = "<font color='red'> Min hole size should be smaller than max hole size.</font>";
					return false;
				}
			 }
		 }
		 
		 var applyDealCounter = 0;
		 $('.voler-deals').each(function() {
			 
			 var vdealApplied = document.getElementById("vDealApplied"+applyDealCounter).checked;
			 
			 var minHoleSize = document.getElementById("minHoleSize"+counterForErrorMsgDiv).value;
			 var minHoleSizeInt = parseInt(minHoleSize);
			 
			 var maxHoleSize = document.getElementById("maxHoleSize"+counterForErrorMsgDiv).value;
			 var maxHoleSizeInt = parseInt(maxHoleSize);
			 
			 if(vdealApplied){
				var count =0;
				$("#loader").show();
				
				var vDealsArray = [];
				var vDealsRangeData = new Array(3);
				
			    $( this ).children().each(function() {
			 	
			    	var dealsrange=($(this).attr("dealsrange"));
			 		var inputCount = 0;

			 		$( this ).children().each(function() {
			 			
			 			if($(this).is('div')){
			 				
			 				$( this ).children().each(function() {
			 					
			 					
			 					if($(this).is('div')){
			 						
			 						$( this ).children().each(function() {
			 							
			 							if($(this).is('div')){
			 								
			 								$( this ).children().each(function() {
			 									
			 									if($(this).is('input')){
									 				
									 				if(inputCount ==3){
										 
										 				inputCount=0;
										 			}
												
									 				vDealsRangeData[inputCount] = $(this).val();
									
													inputCount ++;
									 			
									 			}
			 								});
			 							}
			 						});
			 					}
			 					
			 				});
			 			}
			 			
			 			vDealsArray = vDealsArray.concat(vDealsRangeData);
			 			count++;
			 	});
			 	
			 });
			    
			    document.getElementById('errorMsgDivOfVdeals'+counterForErrorMsgDiv).innerHTML = "";
				 
			    var isVdealApplied = $('#vDealApplied' + counterForErrorMsgDiv).prop("checked");
			    
			    if(isVdealApplied){
			    	
					 if(vDealsArray.length > 0){
						 
						 var dealOffersStartsWithMOB = false;
						 for(var i = 0; i < vDealsArray.length; i++){
							 
							 if(vDealsArray[i] <= 0){
								 document.getElementById('errorMsgDivOfVdeals'+counterForErrorMsgDiv).innerHTML = "<font color='red'> Please enter valid vdeals data.</font>";
								 $("#loader").hide();
								 successStatus = false;
								 
								 return successStatus;
							 }
							 
							 if(i % 3 == 0){
								 
								 if(vDealsArray[i] % 30 != 0){
									 
									 document.getElementById('errorMsgDivOfVdeals'+counterForErrorMsgDiv).innerHTML = "<font color='red'> Minutes should be multiple of 30.</font>";
									 $("#loader").hide();
									 successStatus = false;
									 
									 return successStatus;
								 }
								 
								 if(vDealsArray[i] < minHoleSizeInt){
									 
									 document.getElementById('errorMsgDivOfVdeals'+counterForErrorMsgDiv).innerHTML = "<font color='red'> Minimum minutes should be greater or equal min hole size.</font>";
									 $("#loader").hide();
									 successStatus = false;
									 
									 return successStatus;
								 }
								 
								 if(vDealsArray[i] > maxHoleSizeInt){
									 
									 document.getElementById('errorMsgDivOfVdeals'+counterForErrorMsgDiv).innerHTML = "<font color='red'> Minimum minutes should be less or equal max hole size.</font>";
									 $("#loader").hide();
									 successStatus = false;
									 
									 return successStatus;
								 }
								 
								 
								 if(vDealsArray[i] == minHoleSizeInt){
									 dealOffersStartsWithMOB = true;
								 }
							 }
						 }
						 
						 if(!dealOffersStartsWithMOB){
							 document.getElementById('errorMsgDivOfVdeals'+counterForErrorMsgDiv).innerHTML = "<font color='red'> Deal offers should have a starting range from MOB.</font>";
							 $("#loader").hide();
							 successStatus = false;
							 
							 return successStatus;
						 }
					 }else{
						 
						 $("#loader").hide();
						 document.getElementById('errorMsgDivOfVdeals'+counterForErrorMsgDiv).innerHTML = "<font color='red'> VDeals are required.</font>";
						 successStatus = false;
						 
						 return successStatus;
					 }
			    }
				 
				 counterForErrorMsgDiv++;
		 		}
			});
		
		 return successStatus;
	 }
	 
	 function validateTaxesDetails(){
		 
		 $(".taxesErrorDiv").hide();
		 
		 var numberOfModels=$("#numberOfModels").val();
		 
		 for(var i = 0; i < numberOfModels; i++){
			 
			 var interStateIGST = parseInt(document.getElementById("interStateIGST"+i).value);
			 var interStateCESS = parseInt(document.getElementById("interStateCESS"+i).value);
			 var intraStateCESS = parseInt(document.getElementById("intraStateCESS"+i).value);
			 var intraStateSGST = parseInt(document.getElementById("intraStateSGST"+i).value);
			 var intraStateCGST = parseInt(document.getElementById("intraStateCGST"+i).value);
			 if(interStateIGST < 0 || isNaN(interStateIGST)){
				document.getElementById("taxesErrorDiv"+i).innerHTML = "<font color='red'>Please enter valid Inter State IGST Value</font>";
				$("#taxesErrorDiv"+i).show();
				$("#loader").hide();
				return false;
			 }
			 if(interStateCESS < 0 || isNaN(interStateCESS)){
				document.getElementById("taxesErrorDiv"+i).innerHTML = "<font color='red'>Please enter valid Inter State CESS Value</font>";
				$("#taxesErrorDiv"+i).show();
				$("#loader").hide();
				return false;
			 }
			 
			 if(intraStateCESS < 0 || isNaN(intraStateCESS)){
				document.getElementById("taxesErrorDiv"+i).innerHTML = "<font color='red'>Please enter valid Intra State CESS Value</font>";
				$("#taxesErrorDiv"+i).show();
				$("#loader").hide();
				return false;
			 }
			 if(intraStateSGST < 0 || isNaN(intraStateSGST)){
				document.getElementById("taxesErrorDiv"+i).innerHTML = "<font color='red'>Please enter valid Inter State SGST Value</font>";
				$("#taxesErrorDiv"+i).show();
				$("#loader").hide();
				return false;
			 }
			 if(intraStateCGST < 0 || isNaN(intraStateCGST)){
				document.getElementById("taxesErrorDiv"+i).innerHTML = "<font color='red'>Please enter valid Intra State CGST Value</font>";
				$("#taxesErrorDiv"+i).show();
				$("#loader").hide();
				return false;
			 }
			 return true;
		 }
	 }
 
	 function validateCashBackData(){
			
			$(".cashbackErrorDiv").hide();
			 
			 var numberOfModels=$("#numberOfModels").val();
			 
			 for ( i=0;i<numberOfModels;i++){
			
					var checked = document.getElementById("cashbackApplied"+i).checked;
					
					if(checked){
						
						var duration = document.getElementById("duration"+i).value;
						var amount = document.getElementById("amount"+i).value;
						var max = document.getElementById("max"+i).value;
						
						if(duration <= 0){
							
							document.getElementById("cashbackErrorDiv"+i).innerHTML = "<font color='red'>Please enter valid duration</font>";
							$("#cashbackErrorDiv"+i).show();
							$("#loader").hide();
							return false;
						}
						
						if(duration % 30 != 0){
							
							document.getElementById("cashbackErrorDiv"+i).innerHTML = "<font color='red'>Duration should be multiple of 30</font>";
							$("#cashbackErrorDiv"+i).show();
							$("#loader").hide();
							return false;
						}
						
						if(amount <= 0){
							
							document.getElementById("cashbackErrorDiv"+i).innerHTML = "<font color='red'>Please enter valide amount</font>";
							$("#cashbackErrorDiv"+i).show();
							$("#loader").hide();
							return false;
						}
						
						if(max <= 0){
							
							document.getElementById("cashbackErrorDiv"+i).innerHTML = "<font color='red'>Please enter valide maximum amount</font>";
							$("#cashbackErrorDiv"+i).show();
							$("#loader").hide();
							return false;
						}
					}
			 }
			return true;
		}
	 
	 function validateCarLeftFunctioanlity(){
		 
		 var numberOfModels=$("#numberOfModels").val();
		 var successStatus = true;
		 var counterForErrorMsgDiv = 0;
		 
		 for(var i = 0; i < numberOfModels; i++){
			 document.getElementById("errorMsgDivOfCarLeft"+i).innerHTML = "";
			 
			 var checked = $('#carLeftApplicable' + i).prop('checked');
			 
			 if(checked){
				
				 var minCars=$("#minimumCars"+i).val();
				 var totalLiveCars = $("#liveCarHiddenField"+i).val();
				 
				 if(parseInt(minCars) <= 0){
					 
					document.getElementById("errorMsgDivOfCarLeft"+i).innerHTML = "<font color='red'>Minimum cars can not be zero.</font>";
					$("#errorMsgDivOfCarLeft"+i).show();
					$("#loader").hide();
					return false;
				 }
				 
				 if(parseInt(minCars) > parseInt(totalLiveCars)){
					 
					document.getElementById("errorMsgDivOfCarLeft"+i).innerHTML = "<font color='red'>Minimum cars can not be greater than live cars.</font>";
					$("#errorMsgDivOfCarLeft"+i).show();
					$("#loader").hide();
					return false;
				 }
			 } 
		 }
		 
		 var applyCarLeftCounter = 0;
		 $('.voler-car-left').each(function() {
			 
			 var carLeftApplied = document.getElementById("carLeftApplicable"+applyCarLeftCounter).checked;
			 
			 if(carLeftApplied){
				var count =0;
				$("#loader").show();
				
				var carLeftArray = [];
				var carLeftRangeData = new Array(2);
				
			    $( this ).children().each(function() {
			 	
			    	var dealsrange=($(this).attr("carleftrange"));
			 		var inputCount = 0;

			 		$( this ).children().each(function() {
			 			
			 			if($(this).is('div')){
			 				
			 				$( this ).children().each(function() {
			 					
			 					
			 					if($(this).is('div')){
			 						
			 						$( this ).children().each(function() {
			 							
			 							if($(this).is('div')){
			 								
			 								$( this ).children().each(function() {
			 									
			 									if($(this).is('input')){
									 				
									 				if(inputCount == 3){
										 
										 				inputCount = 0;
										 			}
												
									 				carLeftRangeData[inputCount] = $(this).val();
									
													inputCount ++;
									 			
									 			}else if($(this).is('select')){
									 				

									 				if(inputCount == 3){
										 
										 				inputCount = 0;
										 			}
												
									 				carLeftRangeData[inputCount] = $(this).val();
									
													inputCount ++;
									 			}
			 								});
			 							}
			 						});
			 					}
			 					
			 				});
			 			}
			 			
			 			carLeftArray = carLeftArray.concat(carLeftRangeData);
			 			count++;
			 	});
			 	
			 });
			    
			    document.getElementById('errorMsgDivOfCarLeft'+counterForErrorMsgDiv).innerHTML = "";
			    var iscarLeftFunctionalityApplied = $('#carLeftApplicable' + counterForErrorMsgDiv).prop("checked");
				
			    if(iscarLeftFunctionalityApplied){
				    if(carLeftArray.length > 0){
						 
						 for(var i = 0; i < carLeftArray.length; i = i + 3){
							 
							 if(carLeftArray[i] <= 0){
								 document.getElementById('errorMsgDivOfCarLeft'+counterForErrorMsgDiv).innerHTML = "<font color='red'> Car left perentage can not be zero.</font>";
								 $("#loader").hide();
								 successStatus = false;
								 
								 return successStatus;
							 }
							 
							 if(carLeftArray[i+1] == "" || carLeftArray[i+1] == null){
								 
								 document.getElementById('errorMsgDivOfCarLeft'+counterForErrorMsgDiv).innerHTML = "<font color='red'> Please provide valide notifications.</font>";
								 $("#loader").hide();
								 successStatus = false;
								 
								 return successStatus;
							 }
						 }
					 }else{
						 
						 $("#loader").hide();
						 document.getElementById('errorMsgDivOfCarLeft'+counterForErrorMsgDiv).innerHTML = "<font color='red'> Car left data is required.</font>";
						 successStatus = false;
						 
						 return successStatus;
					 }
			    }
				 
				 counterForErrorMsgDiv++;
		 		}
			});
		 
		 return successStatus;
	 }
 
 </script>
	<script>
$(document).ready(function(){

	if($("#partialPaymentStatus").is(':checked')){
		$("#partialPaymentDiv").show();
	}
	if($("#bookingDurationEligibility").is(':checked')){
		$("#bookingDurationEligibilityDiv").show();
	}
	if($("#bookingPriceEligibility").is(':checked')){
		$("#bookingPriceEligibilityDiv").show();
		}
	
});
</script>

	<script>
		
		$(document).ready(function(){
			var numberOfModels=$("#numberOfModels").val();
			for(i=0;i<numberOfModels;i++){
				
			    var option=$("#selectedValue"+i).val();
			    if(option=="PER_HOUR"){
			    	
			        $("#discountedTariff"+i).hide();
		            $("#regularTariff"+i).hide();
		            $("#specialTariff"+i).show();
		         
		      
		        }
		        else if(option=="FIXED"){
		        	
		
		        	
		            $("#discountedTariff"+i).hide();
		        	$('#specialTariff'+i).hide();
		        	$('#regularTariff'+i).show();
		   
		        }
		       else if(option=="DISCOUNT"){
		        	
		    	   
		        	$('#specialTariff'+i).hide();
		        	$('#regularTariff'+i).hide();
		        	$("#discountedTariff"+i).show();
		   
		        }
			    
			    showHideCashbackDiv(i);
			    
			}
	    var option=$("#selectedValue"+i).val();
	    if(option=="PER_HOUR"){
	    	
	    	$("#perDayPricing"+i).hide();
	        $("#discountedTariff"+i).hide();
            $("#regularTariff"+i).hide();
            $("#specialTariff"+i).show();
         
      
        }
        else if(option=="FIXED"){
        	

        	$("#perDayPricing"+i).hide();
            $("#discountedTariff"+i).hide();
        	$('#specialTariff'+i).hide();
        	$('#regularTariff'+i).show();
   
        }
       else if(option=="DISCOUNT"){
        	
    	   $("#perDayPricing"+i).hide();
        	$('#specialTariff'+i).hide();
        	$('#regularTariff'+i).hide();
        	$("#discountedTariff"+i).show();
        	
   
        }else if(option == "PER_DAY_PRICE"){
        	
        	$('#specialTariff'+i).hide();
        	$('#regularTariff'+i).hide();
        	$("#discountedTariff"+i).hide();
        	$("#perDayPricing"+i).show();
        }
	    
	    
	    
       // else if(option="" || option==null){
        //	break;
        //}
	}
	    
		});
	</script>
	<script>
		
	
	
	    function GetSelectedTextValue(carModel) {
	        var selectedText = carModel.options[carModel.selectedIndex].innerHTML;
	        var selectedValue = carModel.value;
	        var id = $(carModel).attr('id');
	        
	       // selectedValue=id.charAt(id.length-1);
	        selectedValue=id.replace('select','');
	        
	        if(selectedText=="PER_HOUR"){
	        	
	        	$("#discountedTariff"+selectedValue).hide(); 
                $("#regularTariff"+selectedValue).hide();
                $("#perDayPricing"+selectedValue).hide();
                $("#specialTariff"+selectedValue).show();
                
            }
            else if(selectedText=="FIXED"){
     	       
           
            	$('#specialTariff'+selectedValue).hide();
            	$("#discountedTariff"+selectedValue).hide();
            	$("#perDayPricing"+selectedValue).hide();
            	$('#regularTariff'+selectedValue).show();
            }
            else if(selectedText=="DISCOUNT"){
      	       
            	$('#regularTariff'+selectedValue).hide();
            	$('#specialTariff'+selectedValue).hide();
            	$("#perDayPricing"+selectedValue).hide();
            	$("#discountedTariff"+selectedValue).show();
            }else if(selectedText=="PER_DAY_PRICE"){
            	
            	$('#regularTariff'+selectedValue).hide();
            	$('#specialTariff'+selectedValue).hide();
            	$("#discountedTariff"+selectedValue).hide();
            	$("#perDayPricing"+selectedValue).show();
            	
            }else{
            	$('#regularTariff'+selectedValue).hide();
                $('#specialTariff'+selectedValue).hide();
                $("#discountedTariff"+selectedValue).hide();
                $("#perDayPricing"+selectedValue).hide();
            }
               
	        
	    }
	
	</script>

	<script type="text/javascript">
	function getVendorId(){
		  
		  var vendorId =$('#vendorId').val();
		  
		  var redirctToAddCar = ${redirctToAddCar};
		  
		  if(redirctToAddCar){
			  window.location="newCarWithModel?vendorId="+vendorId;
		  }else{
			  window.location="addVendorCarModel?vendorId="+vendorId;  
		  }
		 
		  
	  }
	
	</script>






	<script>
		 
		  
		  function disableBackButton()
			{
				window.history.forward()
			}  
			window.onpageshow=function(evt) { 
				if(evt.persisted)
					disableBackButton() 
			}  
			
			window.onunload=function() { 
				void(0) 
			}
		
		</script>

	<script>
		
			var counterOfVDealsDiv = 1 + ${counterOfVDealsDiv};
			
			function addVDealsOfferDiv(statusIndex,vDealsRangesDivId){
				
				var idOfDiv = 'VDeals' + counterOfVDealsDiv;
				
				var newTextBoxDiv = $(document.createElement('div'))
			     .attr({"id": idOfDiv});
				
				newTextBoxDiv.after().html('<div class="row" id="vDeals">'
						+'<div class="col-md-3">'
						+'<input type="number" name="">'
						+'</div>'
						+'<div class="col-md-3">'
						+'<input type="number" name="">'
						+'</div>'
						+'<div class="col-md-3">'
						+'<input type="number" name="">'
						+'</div>'
						+'<div class="col-md-3">'
						+'<button onclick="removeDiv('+idOfDiv+');"> Remove </button>'
						+'</div>'
						+'</div><br>');
				
				newTextBoxDiv.appendTo("#"+vDealsRangesDivId);
				
				counterOfVDealsDiv++;
			}
		</script>
	<script>
			var counterOfCarLeftDiv = 1 + ${counterOfCarLeftDiv};
			
			function addCarLeftDiv(statusIndex,carLeftRangesDivId){
				
				var idOfDiv = 'CarLeft' + counterOfCarLeftDiv;
				var coloursList = JSON.parse($('#coloursForCarLeftArr').val());
				
				var colorSelectList = "";
				for(var i = 0; i < coloursList.length; i++){
					
					colorSelectList = colorSelectList +
						"<option value='"+coloursList[i]+"'>"+coloursList[i]+"</option>";
				}
				colorSelectList =  "<select class='form-control'>"+colorSelectList+"</select>"
				
				var newTextBoxDiv = $(document.createElement('div'))
			     .attr({"id": idOfDiv});
				
				newTextBoxDiv.after().html('<div class="row">'
						+'<div class="col-md-3">'
						+'<input type="number" name="">'
						+'</div>'
						+'<div class="col-md-3">'
						+'<input type="text" name="">'
						+'</div>'
						+'<div class="col-md-3">'
						+colorSelectList
						+'</div>'
						+'<div class="col-md-3">'
						+'<button onclick="removeDiv('+idOfDiv+');"> Remove </button>'
						+'</div>'
						+'</div><br>');
				
				newTextBoxDiv.appendTo("#"+carLeftRangesDivId);
				
				counterOfCarLeftDiv++;
			}
		</script>

	<script type="text/javascript">
	
	
	var counterOfDiv = 1+${countOfSDDiv};
	 	function addSecurityAmountDiv(statusIndex,SecurityAmountGroupId){
	 		
	 		var idOfDiv = 'SecurityAmountDiv' + counterOfDiv;
	 		
	 		var newTextBoxDiv = $(document.createElement('div'))
		     .attr({"id": idOfDiv, "carmodelindex" : "carModelWithPricing["+statusIndex+"]"});

			newTextBoxDiv.after().html('<input type="text" name="" placeholder="Start Range" >' 
				+ '&nbsp&nbsp-&nbsp&nbsp' + '<input type="text" name="" placeholder="End Range" >'
				+'&nbsp&nbsp&nbsp&nbsp'+'<input type="text" name=""  placeholder="Security Amount" >'
				+'&nbsp<button onclick="removeDiv('+idOfDiv+');"> Remove </button><br><br>');

			newTextBoxDiv.appendTo("#"+SecurityAmountGroupId);
			
			counterOfDiv++;
	 	}
	 	
		
		function removeDiv(idOfDivToRemove){
						
		    $(idOfDivToRemove).remove();
		    
		}
		
		var formNeedToSubmit=false;
		
		function validateIfNeedToSubmit(){
			
			formNeedToSubmit = validateSecurityAmountRange();
			
			if(formNeedToSubmit){
				
				formNeedToSubmit = validateCashBackData();
			}
			
			if(formNeedToSubmit){
				
				formNeedToSubmit = validateVDeal();
			} 
			
			if(formNeedToSubmit){
				
				formNeedToSubmit = validateCarLeftFunctioanlity();
			}
			
			if(formNeedToSubmit){
				
				formNeedToSubmit = validateTaxesDetails();
			}
			
			return formNeedToSubmit;
		}
	
	</script>


	<!-- partial-payment-div-generator -->
	<script type="text/javascript">
	
	
	var countOfPartialPaymentDiv = 1+${countOfPartialPaymentDiv};
	 	function addPartialPaymentDiv(statusIndex,PartialPaymentGroup){
	 		
	 		var idOfDiv = 'PartialPaymentDiv' + countOfPartialPaymentDiv;
	 		
	 		var newTextBoxDiv = $(document.createElement('div'))
		     .attr({"id": idOfDiv, "carmodelindex" : "carModelWithPricing["+statusIndex+"]"});

			newTextBoxDiv.after().html( 
				 '<input type="text" name="" placeholder="Payment Percentage" >'
				+'&nbsp&nbsp'+'<input type="text" name=""  placeholder="Payment Before Booking" >'
				+'&nbsp&nbsp'+'<input type="text" name=""  placeholder="Flexible Payment Duration" >'
				+'&nbsp&nbsp'+'<input type="text" name=""  placeholder="Refund Percentage" >'
				+'&nbsp&nbsp'+'&nbsp<button style="margin-top: 15px;" onclick="removePartialPaymentDiv('+idOfDiv+');"> Remove </button>'
				+'<select class="form-control col-md-3" name=""  style="width: 170px;height: 31px;float: right;"><option>TOTAL_AMOUNT</option><option>TARIFF</option</select><br><br>');

			newTextBoxDiv.appendTo("#"+PartialPaymentGroup);
			
			countOfPartialPaymentDiv++;
	 	}
	 	
		
		function removePartialPaymentDiv(idOfDivToRemove){
						
		    $(idOfDivToRemove).remove();
		    
		}
		
		
		
	
	</script>

	<script>
	
	$("#partialPaymentStatus").click(function() {
		if ($(this).is(':checked')) {
			$("#partialPaymentDiv").show();
		}
		else{
			$("#partialPaymentDiv").hide();
			}
		
       });
	
	$("#bookingDurationEligibility").click(function() {
		if ($(this).is(':checked')) {
			$("#bookingDurationEligibilityDiv").show();
		}
		else{
			$("#bookingDurationEligibilityDiv").hide();
			}
		
     });
	
	$("#bookingPriceEligibility").click(function() {
		if ($(this).is(':checked')) {
			$("#bookingPriceEligibilityDiv").show();
		}
		else{
			$("#bookingPriceEligibilityDiv").hide();
			}
		
  });
	
	
	
	</script>


	<!-- <script>
	$(function (){
	    if($('#partialPaymentStatus').val()== "true"){           
	         $("input:checkbox").prop('checked',true);
	         $("#partialPaymentDiv").show();
	    }else{
	        $("input:checkbox").prop('checked', false);
	    }
	    if($('#bookingPriceEligibility').val()== "true"){           
	         $("input:checkbox").prop('checked',true);
	         $("#bookingPriceEligibilityDiv").show();
	        
	    }else{
	        $("input:checkbox").prop('checked', false);
	    }
	    if($('#bookingDurationEligibility').val()== "true"){           
	         $("input:checkbox").prop('checked',true);
	         $("#bookingDurationEligibilityDiv").show();
	    }else{
	        $("input:checkbox").prop('checked', false);
	    }
	});
	
	
	</script> -->







	<script type="text/javascript">
	
		function notifyForRangeType(idOfLabel,securityAmountRangeType){
			
			var rangeType = document.getElementById(securityAmountRangeType).value;
			
			if(rangeType == "PRICE_RANGE"){
				document.getElementById(idOfLabel).innerHTML = "Provide price range";
			}else if(rangeType == "TIME_RANGE"){
				document.getElementById(idOfLabel).innerHTML = "provide time range<b>(in MINUTES)</b>";
			}
		}
		
		function showHideCashbackDiv(count){
			
			
			var checked = document.getElementById("cashbackApplied"+count).checked;
			
			if(checked){
				$("#cashBackDiv"+count).show();
			}else{
				$("#cashBackDiv"+count).hide();
				document.getElementById("duration"+count).value = 0;
				document.getElementById("amount"+count).value = 0;
				document.getElementById("max"+count).value = 0;
			}
		}
	
		function showHideVdealsDiv(count){
			
			var checked = document.getElementById("vDealApplied"+count).checked;
			
			if(checked){
				$("#vDealsDetails"+count).show();
			}else{
				$("#vDealsDetails"+count).hide();
				document.getElementById("minHoleSize"+count).value = 0;
				document.getElementById("maxHoleSize"+count).value = 0;
				document.getElementById("maxDays"+count).value = 0;
				document.getElementById("vDealsRanges"+count).innerHTML = "";
			}
		}
		
		function showHideCarLeftDiv(id){
			
			var checked = document.getElementById("carLeftApplicable"+id).checked;
			
			if(checked){
				$("#carLeftDiv"+id).show();
			}else{
				$("#carLeftDiv"+id).hide();
				document.getElementById("minimumCars"+id).value = 0;
				document.getElementById("carLeftRanges"+id).innerHTML = "";
			}
		}
	</script>
</body>

<style>
.setWidth {
	width: 100%;
}
</style>

</html>